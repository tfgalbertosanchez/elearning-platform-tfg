package usuarios.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.ApiParam;
import usuarios.controlador.UsuariosServiceController;
import usuarios.security.AvailableRoles;
import usuarios.security.Secured;

@Path("/usuarios")
public class UsuariosApi {

	private final UsuariosApiService service = new UsuariosServiceController();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	//@Secured({AvailableRoles.ADMINISTRADOR})
	public Response createUser(
			@ApiParam(value = "Nombre del usuario", required = true) @FormParam("nombre") String nombre,
			@ApiParam(value = "Apellidos del usuario", required = true) @FormParam("apellidos") String apellidos,	
			@ApiParam(value = "Email", required = true) @FormParam("email") String email,
			@ApiParam(value = "Rol del usuario: administrador, profesor o alumno", required = true) @FormParam("rol") String rol)
			throws NotFoundException {

		return this.service.createUsuario(nombre, apellidos, email, rol);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ADMINISTRADOR, AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getUsers(@Context HttpHeaders httpheaders) throws NotFoundException {
		return service.getUsuarios();
	}

	@GET
	@Path("/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ADMINISTRADOR, AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getUser(
			@ApiParam(value = "Nombre del usuario que se quiere consultar", required = true) @PathParam("username") String username)
			throws NotFoundException {

		return service.getUsuario(username);
	}

	@DELETE
	@Path("/{username}")
	@Secured({AvailableRoles.ADMINISTRADOR})
	public Response removeUser(
			@ApiParam(value = "Nombre del usuario que se quiere eliminar", required = true) @PathParam("username") String username)
			throws NotFoundException {

		return service.removeUsuario(username);
	}
	
}
