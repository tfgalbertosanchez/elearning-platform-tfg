package usuarios.api;

import javax.ws.rs.core.Response;

public interface UsuariosApiService {
	
	public Response createUsuario(String nombre, String apellidos, String email, String rol);
	public Response getUsuario(String username) throws NotFoundException;
	public Response getUsuarios() throws NotFoundException;
	public Response removeUsuario(String username) throws NotFoundException;

}
