package usuarios.model;

public enum Rol {
	ADMINISTRADOR, PROFESOR, ALUMNO;
}
