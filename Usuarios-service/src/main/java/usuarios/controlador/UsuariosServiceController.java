package usuarios.controlador;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;

import usuarios.api.NotFoundException;
import usuarios.api.UsuariosApiService;
import usuarios.dao.FactoriaDAO;
import usuarios.dao.FactoriaJPA;
import usuarios.dao.ResultWrapper;
import usuarios.dao.ResultWrapper.ResultStatus;

public class UsuariosServiceController implements UsuariosApiService {
	
	private FactoriaDAO factoria;

	public UsuariosServiceController() {
		factoria = new FactoriaJPA();
	}

	@Override
	public Response createUsuario(String nombre, String apellidos, String email, String rol) {
		if (StringUtils.isBlank(nombre) || StringUtils.isBlank(apellidos) || StringUtils.isBlank(email)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}
		

		if (StringUtils.isBlank(rol) || (!rol.toUpperCase().equals("PROFESOR") && !rol.toUpperCase().equals("ALUMNO") 
				&& !rol.toUpperCase().equals("ADMINISTRADOR"))) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petici�n son incorrectos: roles disponibles: 'administrador', 'alumno' o ' profesor'")
					.build();
		}

		ResultWrapper result = factoria.getUsuariosDAO().createUsuario(email, nombre, apellidos, email, rol);

		if (result.getStatus() == ResultStatus.EXCEPTION) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(result.getMsg()).build();
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response getUsuario(String username) throws NotFoundException {
		if (StringUtils.isBlank(username)){
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		ResultWrapper result = factoria.getUsuariosDAO().findUsuario(username);

		if (result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response getUsuarios() throws NotFoundException {
		ResultWrapper result = factoria.getUsuariosDAO().findAll();
		return Response.ok().entity(result.getObjs()).build();
	}

	@Override
	public Response removeUsuario(String username) throws NotFoundException {
		if (StringUtils.isBlank(username)){
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		ResultWrapper result = factoria.getUsuariosDAO().removeUsuario(username);

		if (result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}
		
		return Response.status(Status.NO_CONTENT).entity("").build();
	}

}
