package usuarios.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import org.hibernate.Session;

import usuarios.dao.ResultWrapper.ResultStatus;
import usuarios.model.Rol;
import usuarios.model.Usuario;

public class UsuariosJPA implements UsuariosDAO {
	
	private Session session;

	public UsuariosJPA(Session session) {
		this.session = session;
	}
	
	@Override
	public ResultWrapper createUsuario(String username, String nombre, String apellidos, String email, String rol) {
		Usuario u = new Usuario();
		u.setUsername(username);
		u.setNombre(nombre);
		u.setApellidos(apellidos);
		u.setEmail(email);
		u.setRol(Rol.valueOf(rol.toUpperCase()));
		
		try {
			session.beginTransaction();
			session.persist(u);
			session.getTransaction().commit();
			
		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ya existe un usuario con username (email): " + u.getUsername()).build();
		
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + u.toString()).build();
		} finally {
			session.close();
		}
			
		return ResultWrapper.statusOk().setObject(u).build();
	}

	@Override
	public ResultWrapper findUsuario(String username) {
		try{
			Usuario u = session.find(Usuario.class, username);
			if (u == null) {
				return ResultWrapper.statusError().msg("El usuario con username: " + u + " no ha sido encontrado").build();
			}
			
			return ResultWrapper.statusOk().setObject(u).build();
			
		} finally {
			session.close();
		}
	}
	
	@Override
	public ResultWrapper findAll() {
		try {
			List<Usuario> users = session.createQuery("SELECT u FROM Usuario u", Usuario.class).getResultList();
			return ResultWrapper.statusOk().setObjects(users).build();
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper removeUsuario(String username) {
		try {
			Usuario u = session.find(Usuario.class, username);
			
			if (u == null) {
				return ResultWrapper.statusError().msg("El usuario con username: " + u + " no ha sido encontrado").build();
			}
			
			session.beginTransaction();
			session.delete(u);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().build();
		
		} finally {
			session.close();
		}
	}
}
