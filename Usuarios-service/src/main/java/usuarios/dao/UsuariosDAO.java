package usuarios.dao;

public interface UsuariosDAO {
	
	public ResultWrapper createUsuario(String username, String nombre, String apellidos, String email, String rol);
	public ResultWrapper findUsuario(String username);
	public ResultWrapper findAll();
	public ResultWrapper removeUsuario(String username);
	

}
