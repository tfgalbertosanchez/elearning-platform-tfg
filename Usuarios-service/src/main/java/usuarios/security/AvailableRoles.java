package usuarios.security;

public enum AvailableRoles {
	ADMINISTRADOR, PROFESOR, ALUMNO;
}
