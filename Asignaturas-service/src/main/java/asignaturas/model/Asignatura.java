package asignaturas.model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import asignaturas.dao.CustomIdGenerator;


@Entity
@Table(name = "asignaturas")
public class Asignatura {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "asignaturas_seq")
	@GenericGenerator(
			name = "asignaturas_seq", 
			strategy = "asignaturas.dao.CustomIdGenerator",
			parameters = {
					@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "AS") }
	)
	@Column(name = "id_asignatura")
	private String id;
	
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "profesor")
	private String profesor;
	
	@ElementCollection(targetClass=String.class)
	@Column(name = "alumnos")
	private List<String> alumnos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public List<String> getAlumnos() {
		if(alumnos == null) {
			alumnos = new LinkedList<String>();
		}
		
		return alumnos;
	}

	public void setAlumnos(List<String> alumnos) {
		this.alumnos = alumnos;
	}

	@Override
	public String toString() {
		return "Asignatura [id=" + id + ", nombre=" + nombre + ", profesor=" + profesor + ", alumnos=" + alumnos + "]";
	}

}
