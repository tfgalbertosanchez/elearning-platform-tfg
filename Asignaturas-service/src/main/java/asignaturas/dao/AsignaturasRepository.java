package asignaturas.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import asignaturas.model.Asignatura;

public interface AsignaturasRepository extends CrudRepository<Asignatura, String> {
	@Query(value = "SELECT a.id FROM Asignatura a WHERE :userId MEMBER OF a.alumnos")
    ArrayList<String> findAsignaturasByUser(@Param("userId")String userId);
}
