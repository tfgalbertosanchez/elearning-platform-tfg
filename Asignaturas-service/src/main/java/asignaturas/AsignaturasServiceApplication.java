package asignaturas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "asignaturas")
public class AsignaturasServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsignaturasServiceApplication.class, args);
	}

}
