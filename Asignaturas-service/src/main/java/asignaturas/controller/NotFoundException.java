package asignaturas.controller;

public class NotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public NotFoundException(String id) {
		super("Asignatura con id " + id + " no ha sido encontrada.");
	}
}
