package asignaturas.controller;

import java.util.List;

import javax.ws.rs.FormParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import asignaturas.dao.AsignaturasRepository;
import asignaturas.model.Asignatura;
import asignaturas.security.AvailableRoles;
import asignaturas.security.Secured;

@RestController
public class AsignaturasController {
	
	@Autowired
	private AsignaturasRepository asignaturasRepository;
	
	@PostMapping(path = "/asignaturas", produces = "application/json")
	@Secured({AvailableRoles.ADMINISTRADOR})
	public Asignatura createAsignatura(@RequestParam("nombre") String nombre) {
		Asignatura a  = new Asignatura();
		a.setNombre(nombre);
		a.setProfesor("UNASIGNED");
		asignaturasRepository.save(a);
		return a;
	}
	
	@GetMapping(path = "/asignaturas", produces = "application/json")
	@Secured({AvailableRoles.ADMINISTRADOR, AvailableRoles.ALUMNO, AvailableRoles.PROFESOR})
	public List<Asignatura> getAsignaturas() {		
		return (List<Asignatura>) asignaturasRepository.findAll();
	}
	
	@GetMapping(path = "/asignaturas/{idAsignatura}", produces = "application/json")
	@Secured({AvailableRoles.ADMINISTRADOR, AvailableRoles.ALUMNO, AvailableRoles.PROFESOR})
	public Asignatura getAsignatura(@PathVariable String idAsignatura) {
		return asignaturasRepository.findById(idAsignatura)
									.orElseThrow(() -> new NotFoundException(idAsignatura));
	}
	
	@DeleteMapping(path = "/asignaturas/{idAsignatura}")
	@Secured({AvailableRoles.ADMINISTRADOR})
	public void removeAsignatura(@PathVariable String idAsignatura) {
		asignaturasRepository.deleteById(idAsignatura);		
	}
	
	@PutMapping(path = "/asignaturas/{idAsignatura}/docencia", produces = "application/json")
	@Secured({AvailableRoles.ADMINISTRADOR})
	public Asignatura setDocente(@PathVariable String idAsignatura, @FormParam("profesor") String profesor) {
		Asignatura a = asignaturasRepository.findById(idAsignatura)
											.orElseThrow(() -> new NotFoundException(idAsignatura));
		a.setProfesor(profesor);
		asignaturasRepository.save(a);
		return a;
	}
	
	@PutMapping(path = "/asignaturas/{idAsignatura}/matricular", produces = "application/json")
	@Secured({AvailableRoles.ADMINISTRADOR})
	public Asignatura matricular(@PathVariable String idAsignatura, @FormParam("alumno") String alumno) {
		Asignatura a = asignaturasRepository.findById(idAsignatura)
											.orElseThrow(() -> new NotFoundException(idAsignatura));
		a.getAlumnos().add(alumno);
		asignaturasRepository.save(a);
		return a;
	}
	
	@GetMapping(path = "/asignaturas/alumno/{idAlumno}", produces = "application/json")
	@Secured({AvailableRoles.ALUMNO})
	public List<String> getAsignaturasByAlumno(@PathVariable String idAlumno) {	
		List<String> asignaturas = asignaturasRepository.findAsignaturasByUser(idAlumno);
		return asignaturas;
	}

}
