package asignaturas.security;

public enum AvailableRoles {
	ADMINISTRADOR, PROFESOR, ALUMNO;
}
