package asignaturas.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import net.minidev.json.JSONObject;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {

	private static final String SECRET = "01u3J81mMk";
	private static final String BEARER_PREFIX = "Bearer ";

	@Autowired
	private org.springframework.context.ApplicationContext appContext;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
										throws ServletException, IOException {
		// Get the Authorization header from the request
		String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

		// If request does not have Auth header abort request
		if (authorizationHeader == null || !authorizationHeader.startsWith(BEARER_PREFIX)) {
			String msg = buildUnauthorizedMessage("Solicitud sin token de aceso.",
					"unauthorized_client", "Puedes iniciar sesion en http://localhost:8080/login");
			response.resetBuffer();
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.setHeader("Content-Type", "application/json");
			response.getOutputStream().write(msg.getBytes());
			response.flushBuffer(); // marks response as committed
			return;
			
		} else {
			// JWT Token is in the form "Bearer token". Remove Bearer word and get only the
			// Token
			String jwt = authorizationHeader.substring(BEARER_PREFIX.length());

			try {
				// Validate and parse Token
				Claims claims = validateJWT(jwt);

				String userRole = claims.get("rol").toString();
				List<AvailableRoles> roles = getAvailableRoles(request);

				if (!roles.contains(AvailableRoles.valueOf(userRole))) {					
					response.resetBuffer();
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
					response.setHeader("Content-Type", "application/json");
					response.getOutputStream().write(buildForbiddenMessage(userRole).getBytes());
					response.flushBuffer(); // marks response as committed
					return;
				}

			// Catch all validation exceptions and return forbidden
			} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException
					| IllegalArgumentException e) {
				String msg = buildUnauthorizedMessage("El token de acceso no es válido",
													e.getClass().getSimpleName(), e.getMessage());
				response.resetBuffer();
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.setHeader("Content-Type", "application/json");
				response.getOutputStream().write(msg.getBytes());
				response.flushBuffer(); // marks response as committed
				return;
				
			} catch (Exception e) {
				throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
			}
		}
		
		filterChain.doFilter(request, response);
	}
	
	private List<AvailableRoles> getAvailableRoles(HttpServletRequest request) throws Exception{	  
	  RequestMappingHandlerMapping req2HandlerMapping = (RequestMappingHandlerMapping) appContext.getBean("requestMappingHandlerMapping");
	  HandlerExecutionChain handlerExeChain= req2HandlerMapping.getHandler(request); HandlerMethod
	  handlerMethod = (HandlerMethod) handlerExeChain.getHandler();
	  return Arrays.asList(handlerMethod.getMethodAnnotation(Secured.class).value());		 
	}

	private Claims validateJWT(String jwt) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException,
												SignatureException, IllegalArgumentException {
		//This line will throw an exception if it is not a signed JWS (as expected), has malformed claims or is expired
		return Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET)).parseClaimsJws(jwt).getBody();
	}

	private String buildUnauthorizedMessage(String description, String cause, String msg) {
		return new JSONObject().appendField("Error", "Unathorized")
								.appendField("Description", description)
								.appendField("Cause", cause)
								.appendField("Message", msg)
								.toJSONString();		
	}

	private String buildForbiddenMessage(String role) {
		return new JSONObject().appendField("Error", "Forbidden")
								.appendField("Description", "El recurso no esta disponible para el rol: " + role)
								.toJSONString();
	}
}
