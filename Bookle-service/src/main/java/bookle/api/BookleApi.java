package bookle.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import bookle.controller.BookleServiceController;
import bookle.security.AvailableRoles;
import bookle.security.Secured;
import io.swagger.annotations.ApiParam;

@Path("/actividades")
public class BookleApi {

	private final BookleApiService service = new BookleServiceController();	
	
	private static String userId;
	private static String userName;
	private static String authorization;
	
	public static void setUserId(String userId) {
		BookleApi.userId = userId;
	}
	
	public static void setName(String name) {
		BookleApi.userName = name;
	}
	
	public static void setAuthorization(String authorization) {
		BookleApi.authorization = authorization;
	}
	
	public static String getAuthorization() {
		return authorization;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response createActivity(
			@ApiParam(value = "titulo de la actividad", required = true) @FormParam("titulo") String titulo,
			@ApiParam(value = "descripcion de la actividad", required = true) @FormParam("descripcion") String descripcion,
			@ApiParam(value = "codigo de la asignatura", required = true) @FormParam("subjectCode") String subjectCode)
			throws NotFoundException {

		return service.createActivity(titulo, descripcion, userId, subjectCode);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getAtivities() throws NotFoundException {		
		return service.getAtivities(userId);
	}

	@GET
	@Path("/{idActividad}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getActivity(
			@ApiParam(value = "ID de la actividad que se quiere consultar (AC0001)", required = true) @PathParam("idActividad") String idActividad)
			throws NotFoundException {

		return service.getActivity(idActividad, userId);
	}

	@PUT
	@Path("/{idActividad}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response updateActivity(
			@ApiParam(value = "ID de la actividad que se quiere consultar (AC0001)", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "titulo de la actividad", required = true) @FormParam("titulo") String titulo,
			@ApiParam(value = "descripcion de la actividad", required = true) @FormParam("descripcion") String descripcion)
			throws NotFoundException {

		return service.updateActivity(idActividad, titulo, descripcion, userId);
	}

	@DELETE
	@Path("/{idActividad}")
	@Secured({AvailableRoles.PROFESOR})
	public Response removeActivity(
			@ApiParam(value = "ID de la actividad que se quiere consultar (AC0001)", required = true) @PathParam("idActividad") String idActividad)
			throws NotFoundException {

		return service.removeActivity(idActividad, userId);
	}

	@POST
	@Path("/{idActividad}/agenda")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response addActivityDay(
			@ApiParam(value = "ID de la actividad a la que se quiere añadir día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha de la agenda", required = true) @FormParam("fecha") String fecha,
			@ApiParam(value = "Máximo de turno disponibles", required = true) @FormParam("turnos") Integer turnos)
			throws NotFoundException {

		return service.addActivityDay(idActividad, fecha, turnos, userId);
	}

	@GET
	@Path("/{idActividad}/agenda")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getActivityDays(
			@ApiParam(value = "ID de la actividad a la que se quiere consultar agendas (AC0001).", required = true) @PathParam("idActividad") String idActividad)
			throws NotFoundException {

		return service.getActivityDays(idActividad, userId);
	}

	@GET
	@Path("/{idActividad}/{idAgenda}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getActivityDay(
			@ApiParam(value = "ID de la actividad a la que se quiere consultar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos eliminar", required = true) @PathParam("idAgenda") String idAgenda)
			throws NotFoundException {

		return service.getActivityDay(idActividad, idAgenda, userId);
	}
	
	@PUT
	@Path("/{idActividad}/{idAgenda}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response updateActivityDay(
			@ApiParam(value = "ID de la actividad a la que se quiere consultar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos eliminar", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "Máximo de turno disponibles", required = true) @FormParam("turnos") Integer turnos)
			throws NotFoundException{
		
		return service.updateActivityDay(idActividad, idAgenda, turnos, userId);
	}
	

	@DELETE
	@Path("/{idActividad}/{idAgenda}")
	@Secured({AvailableRoles.PROFESOR})
	public Response removeActivityDay(
			@ApiParam(value = "ID de la actividad a la que se quiere eliminar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Id de la agenda que queremos eliminar", required = true) @PathParam("idAgenda") String idAgenda)
			throws NotFoundException {
		
		return service.removeActivityDay(idActividad, idAgenda, userId);
	}
	
	
	@POST
	@Path("/{idActividad}/{idAgenda}/turno")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response addActivityTurn(
			@ApiParam(value = "ID de la actividad a la que se quiere añadir el turno.", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos eliminar", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "Hora de inicio del turno", required = true) @FormParam("inicio") String horaInicio,
			@ApiParam(value = "Hora de fin del turno", required = true) @FormParam("fin") String horaFin)
			throws NotFoundException {
		
		return service.addActivityTurn(idActividad, idAgenda, horaInicio, horaFin, userId);
	}
	
	@GET
	@Path("/{idActividad}/{idAgenda}/turno")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getActivityTurns(
			@ApiParam(value = "ID de la actividad a la que se quiere consultar agendas (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos consultar turnos", required = true) @PathParam("idAgenda") String idAgenda)
			throws NotFoundException {

		return service.getActivityTurns(idActividad, idAgenda, userId);
	}

	@GET
	@Path("/{idActividad}/{idAgenda}/{idTurn}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getActivityTurn(
			@ApiParam(value = "ID de la actividad a la que se quiere consultar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos consultar turnos", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "ID del turno que queremos consultar", required = true) @PathParam("idTurn") String idTurn)
			throws NotFoundException {

		return service.getActivityTurn(idActividad, idAgenda, idTurn, userId);
	}
	
	@PUT
	@Path("/{idActividad}/{idAgenda}/{turno}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response updateActivityTurn(
			@ApiParam(value = "ID de la actividad a la que se quiere eliminar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos eliminar.", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "Identificador del turno a eliminar.", required = true) @PathParam("turno") String idTurn,
			@ApiParam(value = "Hora de inicio del turno", required = true) @FormParam("inicio") String horaInicio,
			@ApiParam(value = "Hora de fin del turno", required = true) @FormParam("fin") String horaFin
			) throws NotFoundException {
		
		return service.updateActivityTurn(idActividad, idAgenda, idTurn, horaInicio, horaFin, userId);
	}
	
	@DELETE
	@Path("/{idActividad}/{idAgenda}/{turno}")
	@Secured({AvailableRoles.PROFESOR})
	public Response removeActivityTurn(
			@ApiParam(value = "ID de la actividad a la que se quiere eliminar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos eliminar.", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "Identificador del turno a eliminar.", required = true) @PathParam("turno") String idTurn) throws NotFoundException {
		
		return service.removeActivityTurn(idActividad, idAgenda, idTurn, userId);
	}
	
	@POST
	@Path("/{idActividad}/{idAgenda}/{turno}/reserva")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response createBooking(
			@ApiParam(value = "ID de la actividad a la que se quiere reservar turno.", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos reservar.", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "Identificado del turno a reservar.", required = true) @PathParam("turno") String idTurn)
			throws NotFoundException {
		
		return service.createBooking(idActividad, idAgenda, idTurn, userName, userId);
	}
	
	@GET
	@Path("/{idActividad}/{idAgenda}/{idTurn}/reserva")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getBooking(
			@ApiParam(value = "ID de la actividad a la que se quiere consultar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos consultar turnos", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "ID del turno que queremos consultar", required = true) @PathParam("idTurn") String idTurn)
			throws NotFoundException {

		return service.getBooking(idActividad, idAgenda, idTurn, userId);
	}
	
	@DELETE
	@Path("/{idActividad}/{idAgenda}/{turno}/reserva")
	@Secured({AvailableRoles.ALUMNO})
	public Response removeBooking(
			@ApiParam(value = "ID de la actividad a la que se quiere eliminar día (AC0001).", required = true) @PathParam("idActividad") String idActividad,
			@ApiParam(value = "Fecha del día que queremos eliminar.", required = true) @PathParam("idAgenda") String idAgenda,
			@ApiParam(value = "Identificador del turno a eliminar.", required = true) @PathParam("turno") String idTurn)
			throws NotFoundException {
		
		return service.removeBooking(idActividad, idAgenda, idTurn, userId);
	}
	
}
