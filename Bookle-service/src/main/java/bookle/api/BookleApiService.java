package bookle.api;

import javax.ws.rs.core.Response;

public interface BookleApiService {
	
	//Manage activities
	public Response createActivity(String titulo, String descripcion, String profesor, String subjectCode) throws NotFoundException;
	public Response getAtivities(String userId) throws NotFoundException;
	public Response getActivity(String idActividad, String userId) throws NotFoundException;
	public Response updateActivity(String idActividad, String titulo, String descripcion, String profesor) throws NotFoundException;
	public Response removeActivity(String idActividad, String userId) throws NotFoundException;
	
	//Manage ActivityDays
	public Response addActivityDay(String idActividad, String fecha, Integer turnos, String userId) throws NotFoundException;
	public Response getActivityDays(String idActividad, String userId) throws NotFoundException;
	public Response getActivityDay(String idActividad, String idAgenda, String userId) throws NotFoundException;
	public Response updateActivityDay(String idActividad, String idAgenda, Integer turnos, String userId) throws NotFoundException;
	public Response removeActivityDay(String idActividad, String idAgenda, String userId) throws NotFoundException;
	
	//Manage Turns
	public Response addActivityTurn(String idActividad, String idAgenda, String horaInicio, String horaFin, String userId) throws NotFoundException;
	public Response getActivityTurns(String idActividad, String idAgenda, String userId) throws NotFoundException;
	public Response getActivityTurn(String idActividad, String idAgenda, String idTurn, String userId) throws NotFoundException;
	public Response updateActivityTurn(String idActividad, String idAgenda, String idTurn, String horaInicio, String horaFin, String userId) throws NotFoundException;
	public Response removeActivityTurn(String idActividad,String idAgenda, String idTurn, String userId) throws NotFoundException;
	
	//Manage Bookings
	public Response createBooking(String idActividad, String idAgenda, String idTurn, String userName, String userId) throws NotFoundException;
	public Response getBooking(String idActividad, String idAgenda, String idTurn, String userId) throws NotFoundException;
	public Response removeBooking(String idActividad, String idAgenda, String idTurn, String userId) throws NotFoundException;
	    

}
