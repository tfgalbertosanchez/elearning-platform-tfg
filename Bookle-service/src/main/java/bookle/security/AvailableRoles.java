package bookle.security;

public enum AvailableRoles {
	ADMINISTRADOR, PROFESOR, ALUMNO;
}
