package bookle.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import bookle.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({"id", "alumno", "email"})
public class Reserva {
	
	@Id	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "reserva_seq")
    @GenericGenerator(
    		name = "reserva_seq",
    		strategy = "bookle.dao.CustomIdGenerator",
    		parameters = {
    				@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "RE") }
    )
	@Column(name = "id_reserva")
	private String id;
	
	@Column(name = "alumno")
	private String alumno = null;

	@Column(name = "email")
	private String userId = null;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_turn")
	private Turno turno;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getAlumno() {
		return alumno;
	}

	public void setAlumno(String alumno) {
		this.alumno = alumno;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Turno getTurno() {
		return turno;
	}
	
	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Reserva {\n");
		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    alumno: ").append(toIndentedString(alumno)).append("\n");
		sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
