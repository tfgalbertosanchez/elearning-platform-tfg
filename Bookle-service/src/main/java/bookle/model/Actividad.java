package bookle.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import bookle.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({"id", "titulo", "descripcion","profesor", "email", "subjectCode", "agendas"})
public class Actividad {
	
	@Id	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "actividad_seq")
    @GenericGenerator(
    		name = "actividad_seq",
    		strategy = "bookle.dao.CustomIdGenerator",
    		parameters = {
    				@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "AC") }
    )
	@Column(name = "id_actividad")
	private String id = null;

	@Column(name = "titulo")
	private String titulo = null;

	@Column(name = "descripcion")
	private String descripcion = null;

	@Column(name = "profesor")
	private String profesor = null;
	
	@ManyToOne(optional = false)
    @JoinColumn(name="subject")
	private AsignaturaInfo subject;

    @OneToMany (cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_actividad")
	private List<Agenda> agendas = null;
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public String getSubjectCode() {
		return subject.getSubjectCode();
	}
	
	@JsonIgnore
	public List<String> getAlumnos(){
		return subject.getAlumnos();
	}
	
	public void setSubject(AsignaturaInfo subject) {
		this.subject = subject;
	}

	public List<Agenda> getAgendas() {
		if(agendas == null) {
			agendas = new ArrayList<Agenda>();
		}
		return agendas;
	}

	public void setAgendas(List<Agenda> agendas) {
		this.agendas = agendas;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Actividad {\n");
		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    titulo: ").append(toIndentedString(titulo)).append("\n");
		sb.append("    descripcion: ").append(toIndentedString(descripcion)).append("\n");
		sb.append("    profesor: ").append(toIndentedString(profesor)).append("\n");
		sb.append("    subjectCode: ").append(toIndentedString(getSubjectCode())).append("\n");
		sb.append("    agendas: ").append(toIndentedString(agendas)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
	
	

}