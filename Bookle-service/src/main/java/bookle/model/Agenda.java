
package bookle.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import bookle.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({ "id", "fecha", "nTurnos", "turnos" })
public class Agenda {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "agenda_seq")
	@GenericGenerator(name = "agenda_seq", strategy = "bookle.dao.CustomIdGenerator", parameters = {
			@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "AG") })
	@Column(name = "id_agenda")
	private String id = null;

	@Column(name = "fecha")
	private String fecha = null;

	@Column(name = "numero")
	private Integer nTurnos = null;

	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_agenda")
	private List<Turno> turnos = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getnTurnos() {
		return nTurnos;
	}

	public void setnTurnos(Integer nTurnos) {
		this.nTurnos = nTurnos;
	}

	public List<Turno> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<Turno> turnos) {
		this.turnos = turnos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Agenda other = (Agenda) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
			
		} else if (!id.equals(other.id))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Agenda {\n");

		sb.append("    fecha: ").append(toIndentedString(fecha)).append("\n");
		sb.append("    nTurnos: ").append(toIndentedString(nTurnos)).append("\n");
		sb.append("    turnos: ").append(toIndentedString(turnos)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
