package bookle.model;


import java.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import bookle.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({"id", "horaInicio", "horaFin", "reserva"})
public class Turno {
	
	@Id	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "turno_seq")
    @GenericGenerator(
    		name = "turno_seq",
    		strategy = "bookle.dao.CustomIdGenerator",
    		parameters = {
    				@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "TU") }
    )
	@Column(name = "id_turno")
	private String id = null;

	@Column(name = "hora_inicio")
	private LocalTime horaInicio = null;
	
	@Column(name = "hora_fin")
	private LocalTime horaFin = null;
	
	@OneToOne(mappedBy = "turno", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private Reserva reserva = null;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getHoraInicio() {
		return horaInicio.toString();
	}
	
	@JsonIgnore
	public LocalTime getHoraInicioLocalTime() {
		return horaInicio;
	}
	
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = LocalTime.parse(horaInicio);
	}
	
	public String getHoraFin() {
		return horaFin.toString();
	}
	
	@JsonIgnore
	public LocalTime getHoraFinLocalTime() {
		return horaFin;
	}
	
	public void setHoraFin(String horaFin) {
		this.horaFin = LocalTime.parse(horaFin);
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Turno other = (Turno) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
			
		} else if (!id.equals(other.id))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Turno {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    horario: ").append(toIndentedString(horaInicio + " - " + horaFin)).append("\n");
		sb.append("    reserva: ").append(toIndentedString(reserva)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
