package bookle.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


public class FactoriaJPA implements FactoriaDAO{

	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;
	
	public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
            	registry = new StandardServiceRegistryBuilder().configure().build();
                MetadataSources sources = new MetadataSources(registry);
                Metadata metadata = sources.getMetadataBuilder().build();
                sessionFactory = metadata.getSessionFactoryBuilder().build();

            } catch (Exception e) {
                e.printStackTrace();
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return sessionFactory;
    }

    public static void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

	@Override
	public ActividadDAO getActividadDAO() {
		return new ActividadJPA(getSessionFactory().openSession());		
	}

	@Override
	public AgendaDAO getAgendaDAO() {
		return new AgendaJPA(getSessionFactory().openSession());
	}

	@Override
	public TurnoDAO getTurnoDAO() {
		return new TurnoJPA(getSessionFactory().openSession());
	}
	
	@Override
	public ReservaDAO getReservaDAO() {
		return new ReservaJPA(getSessionFactory().openSession());
	}
	
	@Override
	public AsignaturaInfoDAO getAsignaturaInfoDAO() {
		return new AsignaturaInfoJPA(getSessionFactory().openSession());
	}
}
