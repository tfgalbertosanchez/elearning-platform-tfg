package bookle.dao;

import java.util.List;

import org.hibernate.Session;

import bookle.dao.ResultWrapper.ResultStatus;
import bookle.model.Actividad;
import bookle.model.Agenda;

public class AgendaJPA implements AgendaDAO {
	
	private Session session;
	
	public AgendaJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createAgenda(String idActividad, String fecha, int turnos, String userId) {
		Actividad ac = session.find(Actividad.class, idActividad);
		
		if (ac == null) {
			session.close();
			return ResultWrapper.statusError()
								.msg("La actividad con id: " + idActividad + " no ha sido encontrada")
								.build();
		}
		
		if(!ac.getProfesor().equals(userId)) {
			session.close();
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
								.msg("La Actividad con id: " + idActividad + " no esta disponible para el usuario: "+ userId)
								.build(); 
		}
		
		//Check for conflict with other days
		String conflict = checkDayConflict(fecha, idActividad);
		
		if(!conflict.isEmpty()) {
			session.close();
			return ResultWrapper.status(ResultStatus.OVERLAP)
								.msg("Ya existe una agenda con fecha " + fecha + " para la actividad " + idActividad + ": " + conflict)
								.build();
		}
		
		//If there are no conflicts insert new Activity Day
		Agenda ag = new Agenda();
		ag.setFecha(fecha);
		ag.setnTurnos(turnos);
		
		ac.getAgendas().add(ag);

		try {
			session.beginTransaction();
			session.persist(ag);
			session.persist(ac);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().setObject(ag).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + ag.toString())
								.build();
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAgendaOfActivity(String idActividad, String idAgenda, String userId) {
		try {
			Actividad ac = session.find(Actividad.class, idActividad);
			
			if (ac == null) {
				return ResultWrapper.statusError()
									.msg("La actividad con id: " + idActividad + " no ha sido encontrada")
									.build();

			} else if(!ac.getProfesor().equals(userId) && !ac.getAlumnos().contains(userId)) {
				session.close();
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Actividad con id: " + idActividad + " no esta disponible para el usuario: "+ userId)
									.build(); 
			}
			
			Agenda ag = session.find(Agenda.class, idAgenda);
			
			if (ag == null) {
				return ResultWrapper.statusError()
									.msg("La agenda con id: " + idAgenda + " no ha sido encontrada")
									.build();
			}
			
			if(!ac.getAgendas().contains(ag)) {
				return ResultWrapper.statusError()
									.msg("La agenda con id: " + idAgenda + " no pertenece a la actividad especificada")
									.build();
			}
			
			return ResultWrapper.statusOk().setObject(ag).build();
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAll(String idActividad, String userId) {
		try {
			Actividad ac = session.find(Actividad.class, idActividad);
			
			if (ac == null) {
				return ResultWrapper.statusError()
									.msg("La actividad con id: " + idActividad + " no ha sido encontrada")
									.build();
			
			} else if(!ac.getProfesor().equals(userId) && !ac.getAlumnos().contains(userId)) {
				session.close();
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Actividad con id: " + idActividad + " no esta disponible para el usuario: "+ userId)
									.build(); 
			}
			
			return ResultWrapper.statusOk().setObjects(ac.getAgendas()).build();
			
		} finally {
			session.close();
		}		
	}
	
	@Override
	public ResultWrapper updateAgenda(String idActividad, String idAgenda, Integer turnos, String userId) {
		//Check if activity and activityDay exist and activityDay belongs to activity
		ResultWrapper rw = findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if(!rw.isStatusOk()) {
			return rw;
		} 
				
		try {
			//Reopen session because findAgendaOfActivity close it
			session = FactoriaJPA.getSessionFactory().openSession();
			
			Agenda ag = session.find(Agenda.class, idAgenda);
			if(turnos != null && turnos != ag.getnTurnos() && turnos > 0) ag.setnTurnos(turnos);
			
			session.beginTransaction();
			session.persist(ag);
			session.getTransaction().commit();
		
			return ResultWrapper.statusOk().setObject(ag).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
					.msg("Ha habido un problema al actualizar el registro. ")
					.build();
			
		} finally {
			session.close();
		}
		
		
	}

	@Override
	public ResultWrapper removeAgenda(String idActividad, String idAgenda, String userId) {
		//Check if activity and activityDay exist and activityDay belongs to activity
		ResultWrapper rw = findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if(!rw.isStatusOk()) {
			return rw;
		} 
		
		try {	
			//Reopen session because findAgendaOfActivity close it
			session = FactoriaJPA.getSessionFactory().openSession();
			
			Agenda ag = session.find(Agenda.class, idAgenda);
			session.beginTransaction();
			session.delete(ag);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().build();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al eliminar el registro.")
								.build();		
		}finally {
			session.close();
		}
	}
	
	private String checkDayConflict(String fecha, String idActividad) {
		List<Agenda> agendas = session.createNativeQuery("SELECT * FROM agenda WHERE fecha = :fecha AND id_actividad = :idAct", Agenda.class)
				.setParameter("fecha", fecha)
				.setParameter("idAct", idActividad)
				.getResultList();
		
		String conflict = "";
		if(!agendas.isEmpty()) {
			conflict += agendas.get(0).getId();
		}
		
		return conflict;
	}

}
