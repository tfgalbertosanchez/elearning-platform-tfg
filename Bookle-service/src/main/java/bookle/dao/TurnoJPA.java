package bookle.dao;

import java.time.LocalTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;

import bookle.dao.ResultWrapper.ResultStatus;
import bookle.model.Agenda;
import bookle.model.Turno;

public class TurnoJPA implements TurnoDAO {

	Session session;

	public TurnoJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createTurno(String idAgenda, String horaInicio, String horaFin) {
		Agenda ag = session.find(Agenda.class, idAgenda);

		if(ag.getTurnos().size() >= ag.getnTurnos()) {
			session.close();
			return ResultWrapper.statusError()
								.msg("La agenda " + idAgenda + " ha alcanzado el maximo de turnos.")
								.build();
		}
		
		Turno turn = new Turno();
		turn.setHoraInicio(horaInicio);
		turn.setHoraFin(horaFin);

		String overlap = checkTimeOverlap(turn.getHoraInicioLocalTime(), turn.getHoraFinLocalTime(), idAgenda);
		
		if(!overlap.isEmpty()) {
			session.close();
			return ResultWrapper.status(ResultStatus.OVERLAP)
								.msg("Error al crear el turno. El horario se superpone con los siguientes turnos: " + overlap)
								.build();
		}

		ag.getTurnos().add(turn);
		
		try {
			session.beginTransaction();
			session.persist(turn);
			session.persist(ag);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().setObject(turn).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + turn.toString())
								.build();
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findTurnOfAgenda(String idAgenda, String idTurno) {
		try {
			//Controller check if activity day and user exists and belongs to Activity
			Agenda ag = session.find(Agenda.class, idAgenda);	
			Turno tu = session.find(Turno.class, idTurno);
	
			if (tu == null) {
				return ResultWrapper.statusError()
									.msg("El turno con id: " + idTurno + " no ha sido encontrado")
									.build();
			}
			
			if(!ag.getTurnos().contains(tu)) {
				return ResultWrapper.statusError()
									.msg("El turno con id: " + idTurno + " no pertenece a la agenda con id: " + idAgenda)
									.build();
			}
			
			return ResultWrapper.statusOk().setObject(tu).build();
		
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAll(String idAgenda) {
		try {
			Agenda ag = session.find(Agenda.class, idAgenda);

			return ResultWrapper.statusOk().setObjects(ag.getTurnos()).build();
			
		}finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper updateTurno(String idAgenda, String idTurno, String horaInicio, String horaFin) {
		//Check if Turn belongs to ActivityDay
		ResultWrapper rw = findTurnOfAgenda(idAgenda, idTurno);		
		if(!rw.isStatusOk()) {
			return rw;
		}
		try {
			//Reopen session because findAgendaOfActivity close it
			session = FactoriaJPA.getSessionFactory().openSession();
			
			Turno turn = session.find(Turno.class, idTurno);
				
			if(StringUtils.isNotBlank(horaInicio) && !turn.getHoraInicio().equals(horaInicio))
				turn.setHoraInicio(horaInicio);
		
			if(StringUtils.isNotBlank(horaFin) && !turn.getHoraFin().equals(horaFin)) 
				turn.setHoraFin(horaFin);
			
			//Check if time update overlap with other turns
			String overlap = checkTimeOverlap(turn.getHoraInicioLocalTime(), turn.getHoraFinLocalTime(), idAgenda);		
			//Delete id if interfere with current turn
			overlap = overlap.replaceAll(idTurno + ", ", "");
		
			if(!overlap.isEmpty()) {
				session.close();
				return ResultWrapper.status(ResultStatus.OVERLAP)
									.msg("Error al crear el turno. El horario se superpone con los siguientes turnos: " + overlap)
									.build();
			}
		
			session.beginTransaction();
			session.persist(turn);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().setObject(turn).build();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro")
								.build();
		} finally {
			session.close();
		}		
	}

	@Override
	public ResultWrapper removeTurno(String idAgenda, String idTurno) {
		//Check if Turn belongs to ActivityDay
		ResultWrapper rw = findTurnOfAgenda(idAgenda, idTurno);		
		if(!rw.isStatusOk()) {
			return rw;
		}
		
		try {
			//Reopen session because findAgendaOfActivity close it
			session = FactoriaJPA.getSessionFactory().openSession();
			
			Turno turn = session.find(Turno.class, idTurno);	
			session.beginTransaction();
			session.delete(turn);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().build();
		
		} finally {
			session.close();
		}
	}
	
	private String checkTimeOverlap(LocalTime horaInicio, LocalTime horaFin, String idAgenda) {
		List<Turno> turnos = session.createNativeQuery("SELECT * FROM turno WHERE hora_inicio < :fin AND hora_fin > :inicio AND id_agenda = :idAg", Turno.class)
									.setParameter("fin", horaFin)
									.setParameter("inicio", horaInicio)
									.setParameter("idAg", idAgenda)
									.getResultList();
		
		String overlappingTurns = "";
		
		if(!turnos.isEmpty()) {
			for(Turno tu : turnos) {
				overlappingTurns += tu.getId() + ", ";
			}
			
			overlappingTurns.substring(0, overlappingTurns.length() - 2);
		}
		
		return overlappingTurns;		
	}

}
