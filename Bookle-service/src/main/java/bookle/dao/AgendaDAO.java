package bookle.dao;

public interface AgendaDAO {
	public ResultWrapper createAgenda(String idActividad, String fecha, int turnos, String userId);
	public ResultWrapper findAgendaOfActivity(String idActividad, String idAgenda, String userId);
	public ResultWrapper findAll(String idActividad, String userId);
	public ResultWrapper updateAgenda(String idActividad, String idAgenda, Integer turnos, String userId);
	public ResultWrapper removeAgenda(String idActividad, String idAgenda, String userId);
}
