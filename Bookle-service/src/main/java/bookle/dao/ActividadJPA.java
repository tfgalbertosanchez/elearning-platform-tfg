package bookle.dao;

import java.util.List;

import org.hibernate.Session;

import bookle.dao.ResultWrapper.ResultStatus;
import bookle.model.Actividad;
import bookle.model.AsignaturaInfo;

public class ActividadJPA implements ActividadDAO {

	private Session session;

	public ActividadJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createActividad(String titulo, String descripcion, String profesor, AsignaturaInfo asignatura) {
		Actividad act = new Actividad();
		act.setTitulo(titulo);
		act.setDescripcion(descripcion);
		act.setProfesor(profesor);
		act.setSubject(asignatura);

		try {
			session.beginTransaction();
			session.persist(act);
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + act.toString())
								.build();		
		} finally {
			session.close();
		}

		return ResultWrapper.statusOk().setObject(act).build();
	}

	@Override
	public ResultWrapper findActividad(String id, String userId) {
		try {
			Actividad ac = session.find(Actividad.class, id);
	
			if (ac == null) {
				return ResultWrapper.statusError().msg("La actividad con id: " + id + " no ha sido encontrada").build();
				
			} else if(!ac.getProfesor().equals(userId) && !ac.getAlumnos().contains(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Actividad con id: " + id + " no esta disponible para el usuario: "+ userId)
									.build();
			}
			
			return ResultWrapper.statusOk().setObject(ac).build();
		
		} finally {
			session.close();
		}		
	}

	@Override
	public ResultWrapper findAll(String userId) {
		try {
			List<Actividad> actividades = session.createQuery("SELECT a FROM Actividad a "
																	  + "WHERE a.profesor = :user " 
																	  + "OR :user MEMBER OF a.subject.alumnos", Actividad.class)
												.setParameter("user", userId)
												.getResultList();
			
			return ResultWrapper.statusOk().setObjects(actividades).build();
		
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper updateActividad(String id, String titulo, String descripcion, String profesor) {
		Actividad act = session.find(Actividad.class, id);

		if (act == null) {
			return ResultWrapper.statusError()
								.msg("La actividad con id: " + id + " no ha sido encontrada")
								.build();
			
		} else if(!act.getProfesor().equals(profesor)) {
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
								.msg("La Actividad con id: " + id + " no esta disponible para el usuario: "+ profesor)
								.build();
		}

		if(titulo != null && !act.getTitulo().equals(titulo)) act.setTitulo(titulo);
		if(descripcion != null && !act.getDescripcion().equals(descripcion)) act.setDescripcion(descripcion);

		try {
			session.beginTransaction();
			session.update(act);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al actualizar el registro: " + act.toString())
								.build();		
		} finally {
			session.close();
		} 

		return ResultWrapper.statusOk().setObject(act).build();
	}

	@Override
	public ResultWrapper removeActividad(String id, String userId) {
		Actividad act = session.find(Actividad.class, id);

		if (act == null) {
			return ResultWrapper.statusError()
								.msg("La actividad con id: " + id + " no ha sido encontrada")
								.build();
			
		} else if(!act.getProfesor().equals(userId)) {
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
								.msg("La Actividad con id: " + id + " no esta disponible para el usuario: "+ userId)
								.build();
		}
		
		try{
			session.beginTransaction();
			session.delete(act);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al actualizar el registro: " + act.toString())
								.build();		
		} finally {
			session.close();
		}

		return ResultWrapper.statusOk().build();
	}

}
