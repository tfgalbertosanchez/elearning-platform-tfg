package bookle.dao;

import org.hibernate.Session;

import bookle.dao.ResultWrapper.ResultStatus;
import bookle.model.Reserva;
import bookle.model.Turno;

public class ReservaJPA implements ReservaDAO {

	private Session session;

	public ReservaJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createReserva(String idTurno, String alumno, String userId) {
		//Controller check if Turn belongs to Activity Day and Activity Day belongs to Activity and User
		Turno tu = session.find(Turno.class, idTurno);
		
		if (tu.getReserva() != null) {
			session.close();
			return ResultWrapper.status(ResultStatus.EXISTS)
								.msg("El turno con id: " + idTurno + " ya ha sido reservado.")
								.build();
		}

		Reserva r = new Reserva();
		r.setAlumno(alumno);
		r.setUserId(userId);
		r.setTurno(tu);
		
		try {
			session.beginTransaction();
			session.persist(r);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().setObject(r).build();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + r.toString())
								.build();
		}finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findReserva(String idTurno, String userId) {
		try {
			//Controller check if Turn belongs to Activity Day and Activity Day belongs to Activity and User
			Turno tu = session.find(Turno.class, idTurno);
			if (tu.getReserva() == null) {
				session.close();
				return ResultWrapper.status(ResultStatus.EXISTS)
									.msg("El turno con id: " + idTurno + " no ha sido reservado.")
									.build();
				
			} else if(!tu.getReserva().getUserId().equals(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Reserva con id: " + tu.getReserva().getId() + " no pertenece al usuario: "+ userId)
									.build();
			}
			
			return ResultWrapper.statusOk().setObject(tu.getReserva()).build();
		
		}finally {
			session.close();
		}
		
	}

	@Override
	public ResultWrapper removeReserva(String idTurno, String userId) {
		try {			
			//Controller check if Turn belongs to Activity Day and Activity Day belongs to Activity and User
			Turno tu = session.find(Turno.class, idTurno);
			if (tu.getReserva() == null) {
				session.close();
				return ResultWrapper.status(ResultStatus.EXISTS)
									.msg("El turno con id: " + idTurno + " no ha sido reservado.")
									.build();
				
			} else if(!tu.getReserva().getUserId().equals(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Reserva con id: " + tu.getReserva().getId() + " no pertenece al usuario: "+ userId)
									.build();
			}
	
			session.beginTransaction();
			session.delete(tu.getReserva());
			tu.setReserva(null);
			session.persist(tu);
			session.getTransaction().commit();
	
			return ResultWrapper.statusOk().build();
		
		} finally {
			session.close();
		}
	}

}
