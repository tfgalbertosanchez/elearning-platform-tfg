package bookle.dao;

public interface ReservaDAO {
	public ResultWrapper createReserva(String idTurno, String alumno, String userId);
	public ResultWrapper findReserva(String idTurno, String userId);
	public ResultWrapper removeReserva(String idTurno, String userId);
}
