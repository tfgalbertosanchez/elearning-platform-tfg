package bookle.dao;

import bookle.model.AsignaturaInfo;

public interface ActividadDAO {
	
	public ResultWrapper createActividad(String titulo, String descripcion, String profesor, AsignaturaInfo asignatura);
	public ResultWrapper findActividad(String id, String userId);
	public ResultWrapper findAll(String userId);
	public ResultWrapper updateActividad(String id, String titulo, String descripcion, String profesor);
	public ResultWrapper removeActividad(String id, String userId);

}
