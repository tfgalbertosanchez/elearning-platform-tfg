package bookle.dao;

public interface TurnoDAO {	
	public ResultWrapper createTurno(String idAgenda, String horaInicio, String horaFin);
	public ResultWrapper findTurnOfAgenda(String idAgenda, String idTurno);
	public ResultWrapper findAll(String idAgenda);
	public ResultWrapper updateTurno (String idAgenda, String idTurno, String horaInicio, String horaFin);
	public ResultWrapper removeTurno (String idAgenda, String idTurno);
}
