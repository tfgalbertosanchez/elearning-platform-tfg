package bookle.dao;

public interface FactoriaDAO {
	
	public ActividadDAO getActividadDAO();
	public AgendaDAO getAgendaDAO();
	public TurnoDAO getTurnoDAO();
	public ReservaDAO getReservaDAO();
	public AsignaturaInfoDAO getAsignaturaInfoDAO();

}
