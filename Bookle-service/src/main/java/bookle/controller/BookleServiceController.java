package bookle.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.NotFoundException;

import bookle.api.BookleApi;
import bookle.api.BookleApiService;
import bookle.dao.FactoriaDAO;
import bookle.dao.FactoriaJPA;
import bookle.dao.ResultWrapper;
import bookle.model.Actividad;
import bookle.model.AsignaturaInfo;
import bookle.rabbitMQ.EventTypes;
import bookle.rabbitMQ.RabbitMQHelper;

public class BookleServiceController implements BookleApiService {

	private static final String ASIGNATURAS_SERVICE_URL = "http://localhost:8094/api/asignaturas/";
	
	private static final String DATE_REGEX = "^(0[1-9]|[12]\\d|3[01])\\/(0[1-9]|1[0-2])\\/(\\d){4}$";
	private static final String TIME_REGEX = "^([01]\\d|2[0-3]):([0-5]\\d):([0-5]\\d)";
	
	private FactoriaDAO factoria;

	public BookleServiceController() {
		factoria = new FactoriaJPA();
	}

	@Override
	public Response createActivity(String titulo, String descripcion, String profesor, String subjectCode)
			throws NotFoundException {

		if (StringUtils.isBlank(titulo) || StringUtils.isBlank(descripcion) || StringUtils.isBlank(profesor) || StringUtils.isBlank(subjectCode)) {

			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}
		
		AsignaturaInfo asignatura = getSubjectInfo(profesor, subjectCode);
		
		if(asignatura == null) {
			return Response.status(Status.FORBIDDEN).entity("El usuario no pertenece a la asignatura " + subjectCode)
					.build();
		}

		ResultWrapper result = factoria.getActividadDAO().createActividad(titulo, descripcion, profesor, asignatura);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}
		
		Actividad act = (Actividad) result.getObj();		
		RabbitMQHelper.publish(EventTypes.NUEVA_TAREA, act.getId(), act.getTitulo(), act.getSubjectCode(), profesor);

		return Response.ok().entity(act).build();
	}

	@Override
	public Response getAtivities(String userId) throws NotFoundException {
		ResultWrapper result = factoria.getActividadDAO().findAll(userId);
		return Response.ok(result.getObjs()).build();
	}

	@Override
	public Response getActivity(String idActividad, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idActividad)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}

		ResultWrapper result = factoria.getActividadDAO().findActividad(idActividad, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}
		
		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response updateActivity(String idActividad, String titulo, String descripcion, String profesor)
			throws bookle.api.NotFoundException {
		
		if (StringUtils.isBlank(idActividad)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		ResultWrapper result = factoria.getActividadDAO().updateActividad(idActividad, titulo, descripcion, profesor);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}
		
		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response removeActivity(String idActividad, String userId) throws bookle.api.NotFoundException {
		if (StringUtils.isBlank(idActividad)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la  son incorrectos").build();
		}

		ResultWrapper result = factoria.getActividadDAO().removeActividad(idActividad, userId);

		if (!result.isStatusOk()) {
			return handleError(result);
		}
			
		RabbitMQHelper.publish(EventTypes.ELIMINAR_TAREA, idActividad, "", "", userId);
		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	@Override
	public Response addActivityDay(String idActividad, String fecha, Integer turnos, String userId) throws bookle.api.NotFoundException {
		
		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(fecha) || !fecha.matches(DATE_REGEX)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos. Date: dd/mm/yyyy").build();
		}
		
		if(turnos == null || turnos <= 0) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos:"
														+ "Debe introducir el numero de turnos disponebles (Min = 1)").build();
		}	

		ResultWrapper result = factoria.getAgendaDAO().createAgenda(idActividad, fecha, turnos, userId);

		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response getActivityDays(String idActividad, String userId) throws bookle.api.NotFoundException {
		if (StringUtils.isBlank(idActividad)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}

		ResultWrapper result = factoria.getAgendaDAO().findAll(idActividad, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.ok(result.getObjs()).build();
	}

	@Override
	public Response getActivityDay(String idActividad, String idAgenda, String userId) throws bookle.api.NotFoundException {
		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}

		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response updateActivityDay(String idActividad, String idAgenda, Integer turnos, String userId)
			throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		ResultWrapper result = factoria.getAgendaDAO().updateAgenda(idActividad, idAgenda, turnos, userId);

		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response removeActivityDay(String idActividad, String idAgenda, String userId) throws bookle.api.NotFoundException {
		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}

		ResultWrapper result = factoria.getAgendaDAO().removeAgenda(idActividad, idAgenda, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	@Override
	public Response addActivityTurn(String idActividad, String idAgenda, String horaInicio, String horaFin, String userId)
			throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(horaInicio)
				|| StringUtils.isBlank(horaFin) || !horaInicio.matches(TIME_REGEX) || !horaFin.matches(TIME_REGEX)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos. Time: hh:mm:ss").build();
		}
		
		// Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}
		
		result = factoria.getTurnoDAO().createTurno(idAgenda, horaInicio, horaFin);

		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response getActivityTurns(String idActividad, String idAgenda, String userId) throws bookle.api.NotFoundException {
		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}
		
		// Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
				
		if (!result.isStatusOk()) {
			return handleError(result);
		}
		
		result = factoria.getTurnoDAO().findAll(idAgenda);
		
		return Response.ok(result.getObjs()).build();
	}

	@Override
	public Response getActivityTurn(String idActividad, String idAgenda, String idTurn, String userId)
			throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(idTurn)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}
		
		// Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
				
		if (!result.isStatusOk()) {
			return handleError(result);
		}	

		result = factoria.getTurnoDAO().findTurnOfAgenda(idAgenda, idTurn);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response updateActivityTurn(String idActividad, String idAgenda, String idTurn, String horaInicio,
			String horaFin, String userId) throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(idTurn) || 
				(StringUtils.isNotBlank(horaInicio) && !horaInicio.matches(TIME_REGEX)) || (StringUtils.isNotBlank(horaFin) && !horaFin.matches(TIME_REGEX))) {
			
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos. Time: hh:mm:ss").build();
		}

		// Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
						
		if (!result.isStatusOk()) {
			return handleError(result);
		}	
		
		result = factoria.getTurnoDAO().updateTurno(idAgenda, idTurn, horaInicio, horaFin);

		if(!result.isStatusOk()) {
			return handleError(result);
		} 

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response removeActivityTurn(String idActividad, String idAgenda, String idTurn, String userId)
			throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(idTurn)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}
		
		// Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
						
		if (!result.isStatusOk()) {
			return handleError(result);
		}	

		result = factoria.getTurnoDAO().removeTurno(idAgenda, idTurn);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}

		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	@Override
	public Response createBooking(String idActividad, String idAgenda, String idTurn, String userName, String userId)
			throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(idTurn)) {			
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}
		
		// 1- Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}	
		
		// 2- Check if Turn exist and belongs to ActivityDay
		result = factoria.getTurnoDAO().findTurnOfAgenda(idAgenda, idTurn);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}	

		// 3- Create booking
		result = factoria.getReservaDAO().createReserva(idTurn, userName, userId);

		if(!result.isStatusOk()) {
			return handleError(result);	
		}
		
		RabbitMQHelper.publish(EventTypes.TAREA_COMPLETADA, idActividad, "", "", userId);
		
		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response getBooking(String idActividad, String idAgenda, String idTurn, String userId) throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(idTurn)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}
		
		// 1- Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}	
		
		// 2- Check if Turn exist and belongs to ActivityDay
		result = factoria.getTurnoDAO().findTurnOfAgenda(idAgenda, idTurn);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}	

		// 3- Get booking
		 result = factoria.getReservaDAO().findReserva(idTurn, userId);

		if(!result.isStatusOk()) {
			return handleError(result);	
		}
		
		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response removeBooking(String idActividad, String idAgenda, String idTurn, String userId)
			throws bookle.api.NotFoundException {

		if (StringUtils.isBlank(idActividad) || StringUtils.isBlank(idAgenda) || StringUtils.isBlank(idTurn)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la peticion son incorrectos").build();
		}

		// 1- Check if Activity and Day exist and if Day and UserId belongs to Activity
		ResultWrapper result = factoria.getAgendaDAO().findAgendaOfActivity(idActividad, idAgenda, userId);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}	
		
		// 2- Check if Turn exist and belongs to ActivityDay
		result = factoria.getTurnoDAO().findTurnOfAgenda(idAgenda, idTurn);
		
		if (!result.isStatusOk()) {
			return handleError(result);
		}	
		
		// 3- Delete booking
		result = factoria.getReservaDAO().removeReserva(idTurn, userId);

		if (!result.isStatusOk()) {
			return handleError(result);
		}
		
		return Response.status(Status.NO_CONTENT).entity("").build();
	}
		
	private AsignaturaInfo getSubjectInfo(String profesor, String subjectCode) {
		AsignaturaInfo subject = factoria.getAsignaturaInfoDAO().findById(subjectCode);
		if(subject == null) {
			return requestAsignaturaInfo(profesor, subjectCode);
			
		} else {
			return subject;
		}
	}
	
	@SuppressWarnings("unchecked")
	private AsignaturaInfo requestAsignaturaInfo(String profesor, String subjectId) {
		String result = executeCallout(ASIGNATURAS_SERVICE_URL + subjectId, BookleApi.getAuthorization());	
		if (result != null) {
			try {
				HashMap<String, Object> resultMap = new ObjectMapper().readValue(result, HashMap.class);
				String profesorId = (String) resultMap.get("profesor");
				if (profesorId.equals(profesor)) {
					return factoria.getAsignaturaInfoDAO().create(resultMap.get("id").toString(), profesorId, (List<String>) resultMap.get("alumnos"));
				}

				return null;

			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	private String executeCallout(String url, String authorization) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader(HttpHeaders.AUTHORIZATION, authorization);
		HttpResponse httpresponse;

		try {
			httpresponse = httpclient.execute(httpget);
			if (httpresponse.getStatusLine().getStatusCode() == HttpServletResponse.SC_OK) {
				String text = new BufferedReader(
						new InputStreamReader(httpresponse.getEntity().getContent(), StandardCharsets.UTF_8)).lines()
								.collect(Collectors.joining("\n"));
				return text;
			}
			return null;

		} catch (UnsupportedOperationException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Response handleError(ResultWrapper rw) {		
		switch (rw.getStatus()) {
			case FORBIDDEN:
				return Response.status(Response.Status.FORBIDDEN).entity(rw.getMsg()).build();
			
			case ERROR:
				return Response.status(Status.NOT_FOUND).entity(rw.getMsg()).build();
			
			case EXCEPTION:
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rw.getMsg()).build();
				
			case OVERLAP:
			case EXISTS:	
				return Response.status(Status.CONFLICT).entity(rw.getMsg()).build();		

			default:
				return null;
		}
	}
	
}
