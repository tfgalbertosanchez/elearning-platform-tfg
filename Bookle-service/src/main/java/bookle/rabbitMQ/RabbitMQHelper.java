package bookle.rabbitMQ;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMQHelper {

	private static ConnectionFactory factory = null;

	private static ConnectionFactory getConnectionFactory() {
		if (factory == null) {
			try {
				factory = new ConnectionFactory();
				factory.setUri("amqp://eqmekjwy:wR_nZWMcUOuLDvqrLuuBxA24l44YtnY4@squid.rmq.cloudamqp.com/eqmekjwy");

			} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e) {
				e.printStackTrace();
				return null;
			}
		}

		return factory;
	}

	public static void publish (EventTypes event, String resourceId, String title, String subjectCode, String userId) { 
		System.out.println("### RabbitMQHelper - createConsumer - EventType: " + event.name());
		
		String exchangeName = "event-exchange";
		String queueName = event.toString();
		String routingKey = queueName;
		
		try {
			Connection connection = getConnectionFactory().newConnection();			
			Channel channel = connection.createChannel();			
			channel.exchangeDeclare(exchangeName, "direct", true);
			
			//parameters: queueName, durable, exclusive, autodelete and properties
			channel.queueDeclare(queueName, true, false, false, null);			
			channel.queueBind(queueName, exchangeName, queueName);
			
			channel.basicPublish(exchangeName, routingKey, 
	                new AMQP.BasicProperties.Builder()
	                    .contentType("application/json")
	                    .build()                
	                , buildMessage(resourceId, title, subjectCode, userId).getBytes());
			
			channel.close();
			connection.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	private static String buildMessage(String resourceId, String title, String subjectCode, String userId){		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			ObjectNode node = mapper.createObjectNode();
	        node.put("service", "Bookle-service");	        
	        node.put("resourceId", resourceId);
	        node.put("title", title);
	        node.put("date", "");
	        node.put("subjectCode", subjectCode);
	        node.put("userId", userId);
	        
	        return mapper.writeValueAsString(node);      
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}		
	}
}
