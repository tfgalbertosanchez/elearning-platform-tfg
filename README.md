# TFG: Aplicación de la arquitectura de microservicios a un sistema de enseñanza virtual #

* Caso de estudio: Plataforma de enseñanza virtual (elearning)
* Autor: Alberto Sánchez Martínez
* Contacto: alberto.sanchez17@um.es

## Acerca del repositorio ##

Este repositorio contiene los proyectos con la implementación de los servicios que conforman el sistema de enseñanza virtual.

Cada microservicio se implementa en su propio proyecto con su propia base de datos y su propio servidor. Para facilitar la exportación 
y ejecución del sistema, todos los servicios utilizan el servidor Jetty como un plugin de Maven y una base de datos en memoria, creando
un entorno autocontenido.

Encontramos los siguientes servicios: 

* **Servicio Usuarios:** Este servicio se encarga de la gestión de los usuarios en el sistema. Para poder utilizar el sistema, los profesores
y alumnos deben ser registrados por un usuario con rol Administrador.

* **Servicio Asignaturas:** Se encarga de dar de alta asignaturas y la funcionalidad para lamatriculación de alumnos y asignación docente del 
profesorado. En este caso, la operación de registrar una asignatura, asignar un docente y matricular un alumno solo estará disponible para
el rolAdministrador. Los usuarios profesor y alumno podrán consultar las asignaturas de las que son docentes o están matriculados.

* **Servicio Bookle:** Se trata de un gestor de reservas. Un profesor puede crear una actividad para una asignatura para la que especifica
una serie de días (agenda) y horas(turnos) en la que los alumnos deberán inscribirse para realizar la actividad. Un ejemplo de una actividad
podría ser la entrevista de una tarea o la revisión de un examen. Los usuarios con rolProfesorpodrán crear una actividad para laasignatura de
la que son docentes y deberá configurar una agenda (con los días en los que desarrollará la actividad) y para cada agenda un número de turnos
(especificando el horario). Los usuarios con rol Alumno que estén matriculados en la asignatura vinculada podrán reservar un turno.

* **Servicio Tareas:** Ofrece la posibilidad de crear una tarea para los alumnos de una asignatura, permite que los alumnos envíen su solución
y que ésta sea calificada por el profesor. La operación para crear una tarea y calificar una solución estará disponible solo para el usuario con
rol profesor que pertenezca a la asignatura asociada y la operación para enviar una solución a una tarea estará disponible solo para los usuarios
con rol Alumno que estén matriculados en la asignaturaasociada.

* **Servicio Agenda:** Es un sistema de recordatorios que un usuario con rol Alumno debe tener en cuenta o hacer. Encontramos dos posibles tipos
de recordatorios: *tareas pendientes* y *fechas clave*. Una *tarea pendiente* es un recordatorio ligado a algo que el usuario deba hacer,
por ejemplo entregar la solución a una tarea, y una *fecha clave* esta relacionado con un una fecha importante para el alumno, como podría ser la
fecha de cierre de una tarea. Los usuarios con rol Alumnosolo tendrán disponibles operaciones de consulta y los usuarios con rol Profesor podrán
tanto crear como consultar *tareas pendientes* y *fechas clave*. Estos recordatorios también serán creados automáticamente cuando se cree una nueva
tarea en el *Servicio Tareas* o una nueva actividad en el *Servicio Bookle*.

* **Servicio Zuul:** Implementa el patrón pasarela de API. El servicio actua de enlace entre el cliente y el sistema. Este servicio se encarga de
la comunicación con el cliente, la redirección de las peticiones al servicio apropiado para su gestión y la autenticación y autorizaciónde los usuarios.


Los servicios utilizanJerseycomo implementación de JAX-RS. Además, los servicios Asignaturas y Zuul han sido implementados utilizando el framework Spring.

## Ejecución del sistema ##

Como cada proyecto esta autocontenido, una vez descargados los proyectos se pueden ejecutar a través del plugin de Maven.

Es necesario descargar instalar [Maven](https://maven.apache.org/download.cgi) para poder ejecutar el servidor en local. Una vez instalado se debe 
configurar la variable de entorno JAVA_HOME con la ruta a la carpeta donde se ubica el JDK (que utilizará maven para compilar y ejecutar el proyecto).
Una vez configurado Maven y la variable de entorno se puede ejecutar los servicios con el comando:

```shell
mvn jetty:run
```

Los servicios que se implementan utilizando el framework Spring se ejecutan con el comando:

```shell
mvn spring-boot:run
```

Las direcciones sobre las que se despliegan los servicios son del tipo *localhost:port*. La configuración de los puertos de cada servicio está definida 
para el caso de aquellos que no utilizan Spring en el fichero *pom.xml* en la declaración del plugin de Jetty y en el caso de los que sí utilizan Spring
en el fichero de configuración *application.yml*. Los servicios se despliegan en las siguientes direcciones: 

| Servicio | Dirección |
| ------------- | ------------- |
| Servicio Zuul | localhost:8080  |
| Servicio Bookle | localhost:8090 |
| Servicio Tareas | localhost:8081  |
| Servicio Agenda | localhost:8092 |
| Servicio Usuarios | localhost:8083  |
| Servicio Asignaturas | localhost:8094 |

En el repositorio se incluye el script __*executer.cmd*__ que ejecuta todos los servicios.

## Especificación de las APIs ##

La especificación de las APIs que exponen los servicios están documentadas con swagger. La documentación de cada una esta accesible a través de los siguientes enlaces:

* Servicio Bookle: https://app.swaggerhub.com/apis-docs/alberto.sanchez17/Bookle-Rest/1.1

* Servicio Tareas: https://app.swaggerhub.com/apis-docs/alberto.sanchez17/Tareas-API/1.0.0

* Servicio Agenda: https://app.swaggerhub.com/apis-docs/albertosanchez/Agenda-service-API/1.0.0

* Servicio Usuarios: https://app.swaggerhub.com/apis-docs/albertosanchez/Usuarios-service-API/1.0.0

* Servicio Asignaturas: https://app.swaggerhub.com/apis-docs/albertosanchez/Asignaturas-Service-API/1.0.0


## Manual de usuario ##

Una vez desplegados todos los servicios, la interacción con el sistema es a través de la pasarela API (Servicio Zuul), por lo tanto todas las peticiones
al sistema se deben hacer a la driección *localhost:8080*. Un ejemplo de una petición para un alumno que quiera consultar la calificación de una solución
a una tarea en el servicio Tareas es el siguiente:

```shell
GET http://localhost:8080/tareas/TA00001/EN00001/calificacion
```

Cuando se intente acceder al sistema sin un token de acceso (*Bearer token*) automatiamente el usuario es redirigido a la página de Google para iniciar 
sesión. El usuario debe haber sido registrado en el sistema por un administrador previamente. Si el login es satisfactorio se genera un token de aceso
para autenticar al usuario con una hora de caducidad. El resultado de iniciar sesión satisfactoriamente es el siguiente: 

```json
{
  "token_type": "Bearer ",
  "expires_in": "3600",
  "status": "Authorized",
  "token": "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMTE1MzI4MTQ0MDQ0NTQ3MjExNjciLCJpc3MiOiJBdXRoZW50aWNhdGlvbiBzZXJ2aWNlIiwiZXhwIjoxNTkyOTMxMjg0LCJzdWIiOiJ1c3VhcmlvYWRtbi50ZmdAZ21haWwuY29tIiwibmFtZSI6IkFkbWluIHRmZyIsInJvbCI6IkFETUlOSVNUUkFET1IifQ.yf1Y8g72FGQ0YMxGabKfGS8ODk8PWInXfmNfWstW5KE"
}
```

Una vez obtenido el token se utilizará en la cabecera *"Authenitcation"* para acceder al sistema. Si el token no es válido el sistema no autoriza al usuario y
le solicita que lo renueve. Un ejemplo de uso con un token caducado es el sigueinte:

```json
{
    "Description": "El token de autenticación no es valido",
    "Message": "JWT expired at 2020-06-21T20:27:29Z. Current time: 2020-06-23T17:56:47Z, a difference of 163758225 milliseconds.  Allowed clock skew: 0 milliseconds.",
    "Cause": "ExpiredJwtException",
    "Error": "Unathorized"
}
```

En primer lugar se debe de registrar un usuario *Administrador* en el servicio Usuarios. Este será el encargado de registrar tanto los usuarios profesores como
alumnos del sistema y registrar las asignaturas. También es el encargado de gestionar la asignación docente de las asignaturas y las matricualciones.

Un usuario, cuando haya iniciado sesión, podrá utilizar las herramientas que le ofrece el sistema.

