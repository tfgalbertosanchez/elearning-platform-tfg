package tareas.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import tareas.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({ "id", "usuario", "fechaEntrega", "solucionTexto", "solucionAdjuntos", "estado", "calificacion" })
public class Envio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "envio_seq")
	@GenericGenerator(name = "envio_seq", strategy = "tareas.dao.CustomIdGenerator", parameters = {
			@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "EN") })
	@Column(name = "id_envio")
	private String id = null;

	@Column(name = "usuario")
	private String usuario = null;

	@Column(name = "fecha_entrega")
	private String fechaEntrega = null;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> solucionAdjuntos = null;

	@Column(name = "solucion_texto")
	@Lob
	private String solucionTexto = null;

	@Column(name = "estado")
	@Enumerated(EnumType.STRING)
	private EstadoEnum estado = null;

	@OneToOne(mappedBy = "envio", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private Calificacion calificacion = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public List<String> getSolucionAdjuntos() {
		return solucionAdjuntos;
	}

	public void setSolucionAdjuntos(List<String> solucionAdjuntos) {
		this.solucionAdjuntos = solucionAdjuntos;
	}

	public String getSolucionTexto() {
		return solucionTexto;
	}

	public void setSolucionTexto(String solucionTexto) {
		this.solucionTexto = solucionTexto;
	}

	public EstadoEnum getEstado() {
		return estado;
	}

	public void setEstado(EstadoEnum estado) {
		this.estado = estado;
	}

	public Calificacion getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(Calificacion calificacion) {
		this.calificacion = calificacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Envio other = (Envio) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
			
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Envio {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    usuario: ").append(toIndentedString(usuario)).append("\n");
		sb.append("    fechaEntrega: ").append(toIndentedString(fechaEntrega)).append("\n");
		sb.append("    solucionAdjuntos: ").append(toIndentedString(solucionAdjuntos)).append("\n");
		sb.append("    solucionTexto: ").append(toIndentedString(solucionTexto)).append("\n");
		sb.append("    estado: ").append(toIndentedString(estado)).append("\n");
		sb.append("    calificacion: ").append(toIndentedString(calificacion)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
