package tareas.model;

public enum TipoCalificacionEnum {
	SIN_CALIFICACION, CALIFICADA;
}