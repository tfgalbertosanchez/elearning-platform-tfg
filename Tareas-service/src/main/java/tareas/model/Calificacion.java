package tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import tareas.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({ "id", "calificador", "puntuacion", "comentarios" })
public class Calificacion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "calificacion_seq")
	@GenericGenerator(
			name = "calificacion_seq", 
			strategy = "tareas.dao.CustomIdGenerator",
			parameters = {
					@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "CA") }
	)
	@Column(name = "id_calificacion")
	private String id;

	@Column(name = "calificador")
	private String calificador = null;

	@Column(name = "puntuacion")
	private float puntuacion;

	@Column(name = "Comentarios")
	private String comentarios = null;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_envio")
	private Envio envio;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCalificador() {
		return calificador;
	}

	public void setCalificador(String calificador) {
		this.calificador = calificador;
	}

	public float getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(float puntuacion) {
		this.puntuacion = puntuacion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	public Envio getEnvio() {
		return envio;
	}
	
	public void setEnvio(Envio envio) {
		this.envio = envio;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Calificacion {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    calificador: ").append(toIndentedString(calificador)).append("\n");
		sb.append("    puntuacion: ").append(toIndentedString(puntuacion)).append("\n");
		sb.append("    comentarios: ").append(toIndentedString(comentarios)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
