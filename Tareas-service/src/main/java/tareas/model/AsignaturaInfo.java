package tareas.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table
public class AsignaturaInfo {
	
	@Id
	@Column(name = "subjectCode")
	private String subjectCode;
	
	@Column(name = "profesor")
	private String profesor;
	
	@ElementCollection(targetClass=String.class)
	@Column(name = "alumnos")	
	private List<String> alumnos;

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public List<String> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(List<String> alumnos) {
		this.alumnos = alumnos;
	}

	@Override
	public String toString() {
		return "AsignaturasInfo [subjectCode=" + subjectCode + ", profesor=" + profesor + ", alumnos=" + alumnos + "]";
	}

}
