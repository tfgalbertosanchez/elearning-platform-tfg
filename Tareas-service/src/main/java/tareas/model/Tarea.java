package tareas.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import tareas.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({ "id", "titulo", "propietario", "openDate", "closeDate", "subjectCode", "permitirTexto", "permitirAdjuntos",
		"totalEnvios", "tipoCalificacion", "instrucciones", "infoAdicional", "idAdjuntos", "accessLevel", "envios" })
public class Tarea {
	
	@Id	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "tarea_seq")
    @GenericGenerator(
    		name = "tarea_seq",
    		strategy = "tareas.dao.CustomIdGenerator",
    		parameters = {
    				@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "TA") }
    )
	@Column(name = "id_tarea")
	private String id = null;

	@Column(name = "titulo")
	private String titulo = null;

	@Column(name = "propietario")
	private String propietario = null;

	@Column(name = "open_date")
	private String openDate = null;

	@Column(name = "close_date")
	private String closeDate = null;
	
	@ManyToOne(optional = false)
    @JoinColumn(name="subject")
	private AsignaturaInfo subject;

	@ElementCollection(fetch = FetchType.EAGER)
	@OrderColumn(name = "attachments")
	private List<String> idAdjuntos = null;

	@Column(name = "permitir_texto")
	private Boolean permitirTexto = null;

	@Column(name = "permitir_adjuntos")
	private Boolean permitirAdjuntos = null;

	@Column(name = "tipo_calificacion")
	@Enumerated(EnumType.STRING)
	private TipoCalificacionEnum tipoCalificacion = null;

	@Column(name = "instrucciones")
	@Lob
	private String instrucciones = null;

	@Column(name = "info_adicional")
	private String infoAdicional = null;

	@Column(name = "accessLevel")
	private String accessLevel = null;

	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tarea")
	@JsonIgnore
	private List<Envio> envios = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	
	public String getSubjectCode() {
		return subject.getSubjectCode();
	}
	
	@JsonIgnore
	public List<String> getAlumnos(){
		return subject.getAlumnos();
	}
	
	public void setSubject(AsignaturaInfo subject) {
		this.subject = subject;
	}

	public List<String> getIdAdjuntos() {
		return idAdjuntos;
	}

	public void setIdAdjuntos(List<String> idAdjuntos) {
		this.idAdjuntos = idAdjuntos;
	}

	public Boolean getPermitirTexto() {
		return permitirTexto;
	}

	public void setPermitirTexto(Boolean permitirTexto) {
		this.permitirTexto = permitirTexto;
	}

	public Boolean getPermitirAdjuntos() {
		return permitirAdjuntos;
	}

	public void setPermitirAdjuntos(Boolean permitirAdjuntos) {
		this.permitirAdjuntos = permitirAdjuntos;
	}

	public TipoCalificacionEnum getTipoCalificacion() {
		return tipoCalificacion;
	}

	public void setTipoCalificacion(TipoCalificacionEnum tipoCalificacion) {
		this.tipoCalificacion = tipoCalificacion;
	}

	public String getInstrucciones() {
		return instrucciones;
	}

	public void setInstrucciones(String instrucciones) {
		this.instrucciones = instrucciones;
	}

	public String getInfoAdicional() {
		return infoAdicional;
	}

	public void setInfoAdicional(String infoAdicional) {
		this.infoAdicional = infoAdicional;
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	public List<Envio> getEnvios() {
		if(envios == null) {
			envios = new ArrayList<Envio>();
		}
		return envios;
	}

	public void setEnvios(List<Envio> envios) {
		this.envios = envios;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Tarea {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    titulo: ").append(toIndentedString(titulo)).append("\n");
		sb.append("    propietario: ").append(toIndentedString(propietario)).append("\n");
		sb.append("    openDate: ").append(toIndentedString(openDate)).append("\n");
		sb.append("    closeDate: ").append(toIndentedString(closeDate)).append("\n");
		sb.append("    subjectCode: ").append(toIndentedString(getSubjectCode())).append("\n");
		sb.append("    idAdjuntos: ").append(toIndentedString(idAdjuntos)).append("\n");
		sb.append("    permitirTexto: ").append(toIndentedString(permitirTexto)).append("\n");
		sb.append("    permitirAdjuntos: ").append(toIndentedString(permitirAdjuntos)).append("\n");
		sb.append("    tipoCalificacion: ").append(toIndentedString(tipoCalificacion)).append("\n");
		sb.append("    instrucciones: ").append(toIndentedString(instrucciones)).append("\n");
		sb.append("    infoAdicional: ").append(toIndentedString(infoAdicional)).append("\n");
		sb.append("    accessLevel: ").append(toIndentedString(accessLevel)).append("\n");
		sb.append("    envios: ").append(toIndentedString(envios)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
