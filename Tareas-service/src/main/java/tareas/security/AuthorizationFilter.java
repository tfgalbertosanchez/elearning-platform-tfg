package tareas.security;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.DatatypeConverter;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.core.util.Priority;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import net.minidev.json.JSONObject;
import tareas.api.TareasApi;

@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter, ResourceFilter {
	private static final String SECRET = "01u3J81mMk";
	private static final String BEARER_PREFIX = "Bearer ";
	
	private final AbstractMethod method;
	
	@Context
    private UriInfo info;

	public AuthorizationFilter(AbstractMethod method) {
		this.method = method;
	}

	@Override
	public ContainerRequest filter(ContainerRequest request) {		
		// Get the Authorization header from the request
        String authorizationHeader = request.getHeaderValue(HttpHeaders.AUTHORIZATION);
		
		// If request does not have Auth header abort request
		if (authorizationHeader == null || !authorizationHeader.startsWith(BEARER_PREFIX)) {
			throw new WebApplicationException(buildUnauthorizedResponse("Solicitud sin token de aceso.",
					"unauthorized_client", "Puedes iniciar sesion en http://localhost:8080/login"));

		} else {
			// JWT Token is in the form "Bearer token". Remove Bearer word and get only the Token
			String jwt = authorizationHeader.substring(BEARER_PREFIX.length());

			try {
				//Validate and parse Token
				Claims claims = validateJWT(jwt);
				
				String userRole = claims.get("rol").toString();
				List<AvailableRoles> roles = Arrays.asList(method.getAnnotation(Secured.class).value());
				
				if(!roles.contains(AvailableRoles.valueOf(userRole))) {
					throw new WebApplicationException(buildForbiddenResponse(userRole));
				}
				
				//If user has a valid role set id in ApiService
				TareasApi.setUserId(claims.getSubject());
				TareasApi.setAuthorization(authorizationHeader);
			
			//Catch all validation exceptions and return forbidden
			} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
				throw new WebApplicationException(buildUnauthorizedResponse("El token de acceso no es valido", e.getClass().getSimpleName(), e.getMessage()));
			}		
		}
		
		return request;		
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		return this;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		return null;
	}

	private Claims validateJWT(String jwt) throws ExpiredJwtException, UnsupportedJwtException, 
													MalformedJwtException, SignatureException, IllegalArgumentException{
		//This line will throw an exception if it is not a signed JWS (as expected), has malformed claims or 
		// is expired
		return Jwts.parser()         
				   .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET))
				   .parseClaimsJws(jwt).getBody();		
	}
	
	private Response buildUnauthorizedResponse(String description, String cause, String msg) {
		String entity =  new JSONObject()
							.appendField("Error", "Unathorized")
							.appendField("Description", description)
							.appendField("Cause", cause)
							.appendField("Message", msg)
							.toJSONString();
		
		return Response.status(Response.Status.UNAUTHORIZED)
					   .header(HttpHeaders.CONTENT_TYPE, "application/json")
					   .entity(entity)
					   .build();
	}
	
	private Response buildForbiddenResponse(String role) {
		String entity =  new JSONObject()
					.appendField("Error", "Forbidden")
					.appendField("Description", "El recurso no esta disponible para el rol: " + role)
					.toJSONString();

		return Response.status(Response.Status.FORBIDDEN)
				   .header(HttpHeaders.CONTENT_TYPE, "application/json")
				   .entity(entity)
				   .build();
	}
}
