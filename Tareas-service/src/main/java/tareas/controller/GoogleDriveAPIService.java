package tareas.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.Collections;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.IOUtils;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.sun.jersey.core.header.FormDataContentDisposition;

public class GoogleDriveAPIService {

	private static final String ACCOUNT_SERVICE_CREDENTIAL = "tareas-service-e1917e3b86d7.json";
	private static final String TASK_ROOT_FOLDER_ID = "1kpyetvYRf1X17B7LC2jl_ZUCygJgbsRX";
	private static final String GOOGLE_DRIVE_BASE_URL = "https://drive.google.com/file/d/";

	public static boolean createTaskFolderStructureGD(String idTask) {
		System.out.println("### START - createTaskFolderStructureGD");

		Drive service = buildDriveService();

		if (service != null) {
			try {
				// 1. Create task folder under root:
				String taskFolder = createFolder(service, idTask, TASK_ROOT_FOLDER_ID);

				// 2. Create attachments folder:
				createFolder(service, "Adjuntos", taskFolder);

				// 3. Create Solutions folder:
				createFolder(service, "Envios", taskFolder);

				// if everything went well return true
				return true;

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

		}

		return false;
	}
	
	public static boolean removeTaskFolders(String idTask) {
		System.out.println("### START - removeTaskFolders");

		Drive service = buildDriveService();

		if (service != null) {
			try {
				// 1. Fetch Task folder id.
				String taskFolderId = getFolderIdByName(service, idTask, TASK_ROOT_FOLDER_ID);
				
				// 2. If exist remove folder
				if(taskFolderId != null) {
					return deleteFile(service, taskFolderId);
				}
				
				return false;
				
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
				
		}
		return false;
	}
	
	public static String uploadTaskAttachment(String idTask, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) {
		System.out.println("### START - uploadTaskAttachment");
		
		Drive service = buildDriveService();

		if (service != null) {
			try {
				// 1. Fetch Task folder id.
				String taskFolderId = getFolderIdByName(service, idTask, TASK_ROOT_FOLDER_ID);

				// 2. Fetch Attachment folder of Task
				String attachmentFolderId = getFolderIdByName(service, "Adjuntos", taskFolderId);
				
				String fileId = UploadFile(service, attachmentFolderId, uploadedInputStream, fileDetail);
				return GOOGLE_DRIVE_BASE_URL + fileId + "/view";
				
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}

	public static String createNewSolution(String idTask, String idSolution, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) {
		System.out.println("### START - createNewSolution");

		Drive service = buildDriveService();

		if (service != null) {
			try {
				// 1. Fetch Task folder id.
				String taskFolderId = getFolderIdByName(service, idTask, TASK_ROOT_FOLDER_ID);

				// 2. Fetch Solutions folder of Task
				String solutionsRootFolderId = getFolderIdByName(service, "Envios", taskFolderId);
				
				// 3. Check if exist a folder for that solution
				String solutionFolder = getFolderIdByName(service, idSolution, solutionsRootFolderId);
				
				// 4. If not exist, create solution folder
				if(solutionFolder == null) {
					solutionFolder = createFolder(service, idSolution, solutionsRootFolderId);
				}

				// 4. upload file to solution folder
				String fileId = UploadFile(service, solutionFolder, uploadedInputStream, fileDetail);
				return GOOGLE_DRIVE_BASE_URL + fileId + "/view";

			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}
	
	public static boolean removeSolutionFolder(String idTask, String idSolution) {
		System.out.println("### START - removeSolutionFolder");

		Drive service = buildDriveService();

		if (service != null) {
			try {
				// 1. Fetch Task folder id.
				String taskFolderId = getFolderIdByName(service, idTask, TASK_ROOT_FOLDER_ID);

				// 2. Fetch Solutions folder of Task
				String solutionsRootFolderId = getFolderIdByName(service, "Envios", taskFolderId);

				// 3. Fetch solution folder
				String solutionFolder = getFolderIdByName(service, idSolution, solutionsRootFolderId);

				// 4.1 If exist, delete solution folder
				if(solutionFolder != null) {
					return deleteFile(service, solutionFolder);
				}
				// 4.2 If not exist, there's nothing to do
				return true;
				
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		return false;
	}

	private static Drive buildDriveService() {
		System.out.println("### START - buildDriveService");
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		try {
			GoogleCredentials googleCredentials = ServiceAccountCredentials
					.fromStream(new FileInputStream(classLoader.getResource(ACCOUNT_SERVICE_CREDENTIAL).getFile()))
					.createScoped(DriveScopes.all());

			HttpRequestInitializer requestInitializer = new HttpCredentialsAdapter(googleCredentials);

			Drive service = new Drive.Builder(GoogleNetHttpTransport.newTrustedTransport(),
					JacksonFactory.getDefaultInstance(), requestInitializer).setApplicationName("TAREAS").build();
			return service;

		} catch (IOException | GeneralSecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static String createFolder(Drive service, String name, String parentFolder) throws IOException {
		System.out.println("### START - createFolder - " + name);
		
		File fileMetadata = new File();
		fileMetadata.setName(name);
		fileMetadata.setParents(Collections.singletonList(parentFolder));
		fileMetadata.setMimeType("application/vnd.google-apps.folder");

		File folder = service.files().create(fileMetadata).setFields("id").execute();

		return folder.getId();
	}

	private static String getFolderIdByName(Drive service, String name, String parentFolder) throws IOException {
		System.out.println("### START - getFolderIdByName - " + name);
		
		String pageToken = null;

		do {
			FileList result = service.files().list()
											 .setQ("mimeType = 'application/vnd.google-apps.folder' and '" + parentFolder + "' in parents")
											 .setSpaces("drive").setFields("nextPageToken, files(id, name)")
											 .setPageToken(pageToken).execute();

			for (File file : result.getFiles()) {
				if (file.getName().equals(name)) {
					return file.getId();
				}
			}

			pageToken = result.getNextPageToken();

		} while (pageToken != null);

		return null;
	}

	private static String UploadFile(Drive service, String parentFolder, InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail) {
		System.out.println("### START - UploadFile - " + fileDetail.getFileName());
		
		File fileMetadata = new File();
		fileMetadata.setName(fileDetail.getFileName());
		fileMetadata.setParents(Collections.singletonList(parentFolder));
		
		try {
			//Create a temp file with the upload data
			java.io.File filepath = new java.io.File(fileDetail.getFileName());
			OutputStream outputStream = new FileOutputStream(filepath);
			IOUtils.copy(uploadedInputStream, outputStream);
		
			FileContent mediaContent = new FileContent(Files.probeContentType(filepath.toPath()), filepath);
			File file = service.files().create(fileMetadata, mediaContent).setFields("id").execute();
			
			//Delete temp file
			outputStream.close();
			filepath.delete();
			
			return file.getId();
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}	

	}

	// If folder, delete all files under it
	private static boolean deleteFile(Drive service, String googleId) {
		System.out.println("### START - deleteFile - " + googleId);
				
		try {
			service.files().delete(googleId).execute();
			return true;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
