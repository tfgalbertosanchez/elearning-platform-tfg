package tareas.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.core.header.FormDataContentDisposition;

import tareas.api.NotFoundException;
import tareas.api.TareasApi;
import tareas.api.TareasApiService;
import tareas.dao.FactoriaDAO;
import tareas.dao.FactoriaJPA;
import tareas.dao.ResultWrapper;
import tareas.dao.ResultWrapper.ResultStatus;
import tareas.model.AsignaturaInfo;
import tareas.model.Envio;
import tareas.model.Tarea;
import tareas.rabbitMQ.EventTypes;
import tareas.rabbitMQ.RabbitMQHelper;

public class TareaServiceController implements TareasApiService {

	private static final String ASIGNATURAS_SERVICE_URL = "http://localhost:8094/api/asignaturas/";
	
	private static final String DATE_REGEX = "^(0[1-9]|[12]\\d|3[01])\\/(0[1-9]|1[0-2])\\/(\\d){4}$";

	private FactoriaDAO factoria;

	public TareaServiceController() {
		factoria = new FactoriaJPA();
	}

	@Override
	public Response createTask(String titulo, String propietario, String openDate, String closeDate, String subjectCode, String permiteTexto, String permitirAdjuntos,
			String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel, InputStream uploadedInputStream, FormDataContentDisposition fileDetail)
			throws NotFoundException {

		if (StringUtils.isBlank(titulo) || StringUtils.isBlank(propietario) || StringUtils.isBlank(openDate)|| !openDate.matches(DATE_REGEX)
				 || StringUtils.isBlank(closeDate) || !closeDate.matches(DATE_REGEX) || StringUtils.isBlank(subjectCode)) {

			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos. Formato fechas: dd/mm/yyyy").build();
		}
		
		AsignaturaInfo subject = getSubjectInfo(propietario, subjectCode);

		if (subject == null) {
			return Response.status(Status.FORBIDDEN).entity("El usuario no pertenece a la asignatura " + subjectCode)
					.build();
		}

		ResultWrapper result = factoria.getTareaDAO().createTarea(titulo, propietario, openDate, closeDate, subject,
				Boolean.parseBoolean(permiteTexto),	Boolean.parseBoolean(permitirAdjuntos), tipoCalificacion, instrucciones, infoAdicional, accessLevel);
		
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		Tarea task = (Tarea) result.getObj();
		String taskId = task.getId();
		
		if(!GoogleDriveAPIService.createTaskFolderStructureGD(taskId)){
			return Response.status(502).entity("Error al crear la estructura de carpetas para el almacenamiento de documentos").build();
		}
		
		if(uploadedInputStream != null && fileDetail !=null) {	
			String googleAttachmentId = GoogleDriveAPIService.uploadTaskAttachment(taskId, uploadedInputStream, fileDetail);	
			result = factoria.getTareaDAO().updateAttachmentField(taskId, googleAttachmentId);
		}
		
		
		RabbitMQHelper.publish(EventTypes.NUEVA_FECHA, taskId, task.getTitulo(), task.getCloseDate(), task.getSubjectCode(), propietario);
		RabbitMQHelper.publish(EventTypes.NUEVA_TAREA, taskId, task.getTitulo(), "", task.getSubjectCode(), propietario);

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response getTasks(String userId) throws NotFoundException {
		ResultWrapper result = factoria.getTareaDAO().findAllTareas(userId);
		return Response.ok(result.getObjs()).build();
	}

	@Override
	public Response getTask(String idTarea, String userId) throws NotFoundException {
		ResultWrapper result = factoria.getTareaDAO().findTareaById(idTarea, userId);
		
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response updateTask(String idTarea, String titulo, String propietario, String openDate, String closeDate, String permiteTexto, String permitirAdjuntos,
			String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel, InputStream uploadedInputStream, FormDataContentDisposition fileDetail)
			throws NotFoundException {

		if (StringUtils.isBlank(idTarea) || (StringUtils.isNotBlank(openDate) && !openDate.matches(DATE_REGEX)) 
												|| (StringUtils.isNotBlank(closeDate) && !closeDate.matches(DATE_REGEX))) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos. Formato fechas: dd/mm/yyyy").build();
		}
		
		//Check if Task exist before update
		ResultWrapper result = factoria.getTareaDAO().findTareaById(idTarea, propietario);
				
		if(result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}
				
		String adjuntoId = "";
		if(uploadedInputStream != null && fileDetail !=null) {
			adjuntoId = GoogleDriveAPIService.uploadTaskAttachment(idTarea, uploadedInputStream, fileDetail);
		}
		
		result = factoria.getTareaDAO().updateTarea(idTarea, titulo, propietario, openDate, closeDate, adjuntoId,
				permiteTexto, permitirAdjuntos, tipoCalificacion, instrucciones, infoAdicional, accessLevel);
		
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response removeTask(String idTarea, String userId) throws NotFoundException {
		if(StringUtils.isBlank(idTarea)) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		ResultWrapper result = factoria.getTareaDAO().remove(idTarea, userId);
		
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		if(!GoogleDriveAPIService.removeTaskFolders(idTarea)) {
			return Response.status(502).entity("Error al eliminar la estructura de carpetas de la tarea " + idTarea).build();
		}
		
		RabbitMQHelper.publish(EventTypes.ELIMINAR_FECHA, idTarea, "", "", "", userId);
		RabbitMQHelper.publish(EventTypes.ELIMINAR_TAREA, idTarea, "", "", "", userId);

		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	@Override
	public Response createSolution(String idTarea, String usuario, String solucionTexto, InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail) throws NotFoundException {
		
		if (StringUtils.isBlank(idTarea) || StringUtils.isBlank(usuario) ) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
				
		ResultWrapper result = factoria.getEnvioDAO().createEnvio(idTarea, usuario, solucionTexto);
		
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		Tarea ta = (Tarea) factoria.getTareaDAO().findTareaById(idTarea, usuario).getObj();
		
		if(ta.getPermitirAdjuntos() && uploadedInputStream != null && fileDetail !=null) {
			
			String solutionId = ( (Envio)result.getObj() ).getId();			
			String googleSolutionId = GoogleDriveAPIService.createNewSolution(idTarea, solutionId, uploadedInputStream, fileDetail);			
			result = factoria.getEnvioDAO().updateAttachmentField(solutionId, googleSolutionId);
		}

		RabbitMQHelper.publish(EventTypes.TAREA_COMPLETADA, idTarea, "", "", "", usuario);
		
		return Response.ok().entity(result.getObj()).build();		
	}

	@Override
	public Response getSolutions(String idTarea, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idTarea)) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		ResultWrapper result = factoria.getEnvioDAO().findAllEnvios(idTarea, userId);
				
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}

		return Response.ok().entity(result.getObjs()).build();
	}

	@Override
	public Response getSolution(String idTarea, String idSolucion, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idTarea) || StringUtils.isBlank(idSolucion) ) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		ResultWrapper result = factoria.getEnvioDAO().findEnvioInTask(idTarea, idSolucion, userId);
				
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response updateSolution(String idTarea, String idSolucion, String solucionTexto, InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idTarea) || StringUtils.isBlank(idSolucion) ) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		//Check if Task and Solution exist before update
		ResultWrapper result = factoria.getTareaDAO().findTareaById(idTarea, null);
		
		if(result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		Tarea ta = (Tarea) result.getObj();
		
		result = factoria.getEnvioDAO().findEnvioInTask(idTarea, idSolucion, userId);
			
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}
		
		//If exist upload attachment and update solution		
		String adjuntoId = "";
		if(ta.getPermitirAdjuntos() && uploadedInputStream != null && fileDetail !=null) {
			adjuntoId = GoogleDriveAPIService.createNewSolution(idTarea, idSolucion, uploadedInputStream, fileDetail);
		}
		
		result = factoria.getEnvioDAO().updateEnvio(idTarea, idSolucion, userId, adjuntoId, solucionTexto);
				
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response createGrade(String idTarea, String idSolucion, String calificador, float puntuacion,
			String comentarios) throws NotFoundException {
		if (StringUtils.isBlank(idTarea) || StringUtils.isBlank(idSolucion)) {
			return Response.status(Status.BAD_REQUEST)
							.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		ResultWrapper result = factoria.getCalificacionDAO().createCalificacion(idTarea, idSolucion, calificador, puntuacion, comentarios);
				
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response getGrade(String idTarea, String idSolucion, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idTarea) || StringUtils.isBlank(idSolucion) ) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		ResultWrapper result = factoria.getCalificacionDAO().findCalificacionBySolution(idTarea, idSolucion, userId);
				
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response updateGrade(String idTarea, String idSolucion, float puntuacion, String comentarios, String userId)
			throws NotFoundException {
		if (StringUtils.isBlank(idTarea) || StringUtils.isBlank(idSolucion) ) {
			return Response.status(Status.BAD_REQUEST)
					.entity("Los parametros de la petición son incorrectos.").build();
		}
		
		ResultWrapper result = factoria.getCalificacionDAO().updateCalificacion(idTarea, idSolucion, puntuacion, comentarios, userId);
				
		if (result.getStatus() != ResultStatus.OK) {
			return handleError(result);
		}

		return Response.ok().entity(result.getObj()).build();
	}
	
	
	private AsignaturaInfo getSubjectInfo(String profesor, String subjectCode) {
		AsignaturaInfo subject = factoria.getAsignaturaInfoDAO().findById(subjectCode);
		if(subject == null) {
			return requestAsignaturaInfo(profesor, subjectCode);
			
		} else {
			return subject;
		}
	}
	
	@SuppressWarnings("unchecked")
	private AsignaturaInfo requestAsignaturaInfo(String profesor, String subjectId) {
		String result = executeCallout(ASIGNATURAS_SERVICE_URL + subjectId, TareasApi.getAuthorization());	
		if (result != null) {
			try {
				HashMap<String, Object> resultMap = new ObjectMapper().readValue(result, HashMap.class);
				String profesorId = (String) resultMap.get("profesor");
				if (profesorId.equals(profesor)) {
					return factoria.getAsignaturaInfoDAO().create(resultMap.get("id").toString(), profesorId, (List<String>) resultMap.get("alumnos"));
				}

				return null;

			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	private String executeCallout(String url, String authorization) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader(HttpHeaders.AUTHORIZATION, authorization);
		HttpResponse httpresponse;

		try {
			httpresponse = httpclient.execute(httpget);
			if (httpresponse.getStatusLine().getStatusCode() == HttpServletResponse.SC_OK) {
				String text = new BufferedReader(
						new InputStreamReader(httpresponse.getEntity().getContent(), StandardCharsets.UTF_8)).lines()
								.collect(Collectors.joining("\n"));
				return text;
			}
			return null;

		} catch (UnsupportedOperationException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Response handleError(ResultWrapper rw) {		
		if (rw.getStatus() == ResultStatus.FORBIDDEN) {
			return Response.status(Response.Status.FORBIDDEN).entity(rw.getMsg()).build();
			
		} else if(rw.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(rw.getMsg()).build();
			
		} else if (rw.getStatus() == ResultStatus.EXCEPTION) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rw.getMsg()).build();

		} else {
			return null;
		}
	}	
}
