package tareas.dao;

import java.util.Date;

import org.hibernate.Session;

import tareas.dao.ResultWrapper.ResultStatus;
import tareas.model.Envio;
import tareas.model.EstadoEnum;
import tareas.model.Tarea;

public class EnvioJPA implements EnvioDAO {

	private Session session;

	public EnvioJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createEnvio(String idTarea, String usuario, String solucionTexto) {
		Tarea ta = session.find(Tarea.class, idTarea);
		if(ta == null) {
			session.close();
			return ResultWrapper.statusError()
					.msg("La tarea con id: " + idTarea + " no ha sido encontrada")
					.build();
		}
		
		if(!ta.getAlumnos().contains(usuario)) {
			session.close();
			 return ResultWrapper.status(ResultStatus.FORBIDDEN)
					.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ usuario)
					.build();
		}
		
		Envio en = new Envio();
		en.setUsuario(usuario);
		if (ta.getPermitirTexto()) 	en.setSolucionTexto(solucionTexto);
		en.setFechaEntrega(new Date().toString());
		en.setEstado(EstadoEnum.NO_CALIFICADA);		
		
		ta.getEnvios().add(en);
		
		session.beginTransaction();
		try {
			session.persist(en);
			session.persist(ta);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + en.toString())
								.build();
		} finally {
			session.close();
		}

		return ResultWrapper.statusOk().setObject(en).build();
	}

	@Override
	public ResultWrapper findEnvioInTask(String idTarea, String idEnvio, String userId) {
		try{
			Tarea ta = session.find(Tarea.class, idTarea);
			
			if(ta == null) {
				return ResultWrapper.statusError()
									.msg("La tarea con id: " + idTarea + " no ha sido encontrada")
									.build();
			}
			
			Envio en = session.find(Envio.class, idEnvio);
			
			if(en == null) {
				return ResultWrapper.statusError()
									.msg("El envio con id: " + idEnvio + " no ha sido encontrado")
									.build();
			}
			
			if(!en.getUsuario().equals(userId)) {
				 return ResultWrapper.status(ResultStatus.FORBIDDEN)
							.msg("El envio con id: " + idEnvio + " no pertenece al usuario: " + userId)
							.build();
			}
			
			if(!ta.getEnvios().contains(en)) {
				return ResultWrapper.statusError()
									.msg("El envio con id: " + idEnvio + " no pertenece a la tarea especificada")
									.build();
			}
			
			return ResultWrapper.statusOk().setObject(en).build();
		
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAllEnvios(String idTarea, String userId) {
		try {
	 		Tarea ta = session.find(Tarea.class, idTarea);
			
			if(ta == null) {
				return ResultWrapper.statusError()
						.msg("La tarea con id: " + idTarea + " no ha sido encontrada")
						.build();
			}
			
			if(!ta.getPropietario().equals(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
						.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ userId)
						.build();
			}
			
			return ResultWrapper.statusOk().setObjects(ta.getEnvios()).build();
	
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper updateEnvio(String idTarea, String idEnvio, String usuario, String idAdjunto, String newText) {
		//Controller check if task and solution exist and solution belongs to task
		Tarea ta = session.find(Tarea.class, idTarea);	
		Envio en = session.find(Envio.class, idEnvio);
		
		if(!en.getUsuario().equals(usuario)) {
			session.close();
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
					.msg("El envio con id: " + idEnvio + " no pertenece al usuario: "+ usuario)
					.build();
		}
		
		if(ta.getPermitirTexto() && !en.getSolucionTexto().equals(newText)) en.setSolucionTexto(newText);
		en.getSolucionAdjuntos().add(idAdjunto);
		
		session.beginTransaction();
		try {
			session.persist(en);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + en.toString())
								.build();
		} finally {
			session.close();
		}

		return ResultWrapper.statusOk().setObject(en).build();
	}

	@Override
	public ResultWrapper updateAttachmentField(String idEnvio, String idAdjunto) {
		Envio en = session.find(Envio.class, idEnvio);
		
		en.getSolucionAdjuntos().add(idAdjunto);
		
		session.beginTransaction();
		try {
			session.persist(en);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + en.toString())
								.build();
		} finally {
			session.close();
		}

		return ResultWrapper.statusOk().setObject(en).build();
		
	}	

}
