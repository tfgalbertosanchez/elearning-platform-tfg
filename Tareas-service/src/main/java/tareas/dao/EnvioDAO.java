package tareas.dao;

public interface EnvioDAO {
	public ResultWrapper createEnvio(String idTarea, String usuario, String solucionTexto);
	public ResultWrapper findEnvioInTask(String idTarea, String idEnvio, String userId);
	public ResultWrapper findAllEnvios(String idTarea, String userId);
	public ResultWrapper updateEnvio(String idTarea, String idEnvio, String usuario, String idAdjunto, String newText);
	public ResultWrapper updateAttachmentField(String idEnvio, String idAdjunto);
}
