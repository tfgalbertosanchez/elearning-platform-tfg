package tareas.dao;

import tareas.model.AsignaturaInfo;

public interface TareaDAO {
	
	public ResultWrapper createTarea(String titulo, String propietario, String openDate, String closeDate, AsignaturaInfo subject, Boolean permiteTexto, Boolean permitirAdjuntos , String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel);
	public ResultWrapper findTareaById(String idTarea, String userId);
	public ResultWrapper findAllTareas(String userId);
	public ResultWrapper updateTarea(String idTarea, String titulo, String propietario, String openDate, String closeDate, String idAdjunto, String permiteTexto, String permitirAdjuntos, String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel);
	public ResultWrapper remove(String idTarea, String userId);
	public ResultWrapper updateAttachmentField(String idTarea, String idAdjunto);

}
