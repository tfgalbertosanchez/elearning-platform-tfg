package tareas.dao;

import java.util.List;

import org.hibernate.Session;

import tareas.model.AsignaturaInfo;

public class AsignaturaInfoJPA implements AsignaturaInfoDAO {
	
	private Session session;
	
	public AsignaturaInfoJPA(Session session) {
		this.session = session;
	}

	@Override
	public AsignaturaInfo create(String subjectCode, String profesor, List<String> alumnos) {
		AsignaturaInfo asignatura = new AsignaturaInfo();
		asignatura.setSubjectCode(subjectCode);
		asignatura.setProfesor(profesor);
		asignatura.setAlumnos(alumnos);
		
		try {
			session.beginTransaction();
			session.persist(asignatura);
			session.getTransaction().commit();
			
			return asignatura;
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return null;
		
		} finally {
			session.close();
		}
	}

	@Override
	public AsignaturaInfo findById(String id) {
		AsignaturaInfo asignatura = session.find(AsignaturaInfo.class, id);
		return asignatura;
	}

}
