package tareas.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class FactoriaJPA implements FactoriaDAO{

	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;
		
	public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
            	registry = new StandardServiceRegistryBuilder().configure().build();
                MetadataSources sources = new MetadataSources(registry);
                Metadata metadata = sources.getMetadataBuilder().build();
                sessionFactory = metadata.getSessionFactoryBuilder().build();

            } catch (Exception e) {
                e.printStackTrace();
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return sessionFactory;
    }

    public static void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

	@Override
	public TareaDAO getTareaDAO() {		
		return new TareaJPA(getSessionFactory().openSession());
	}

	@Override
	public EnvioDAO getEnvioDAO() {
		return new EnvioJPA(getSessionFactory().openSession());
	}

	@Override
	public CalificacionDAO getCalificacionDAO() {
		return new CalificacionJPA(getSessionFactory().openSession());
	}
	
	@Override
	public AsignaturaInfoDAO getAsignaturaInfoDAO() {
		return new AsignaturaInfoJPA(getSessionFactory().openSession());
	}

}
