package tareas.dao;

public interface CalificacionDAO {
	public ResultWrapper createCalificacion(String idTarea, String idSolucion, String calificador, float puntuacion, String comentarios);
	public ResultWrapper findCalificacionBySolution(String idTask, String idSolucion, String userId);
	public ResultWrapper updateCalificacion(String idTask, String idSolucion, float puntuacion, String comentarios, String userId);
}
