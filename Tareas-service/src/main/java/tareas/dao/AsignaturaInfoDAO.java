package tareas.dao;

import java.util.List;

import tareas.model.AsignaturaInfo;

public interface AsignaturaInfoDAO {
	public AsignaturaInfo create(String subjectCode, String profesor, List<String> alumnos);
	public AsignaturaInfo findById(String Id);
}
