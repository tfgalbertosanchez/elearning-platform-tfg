package tareas.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;

import tareas.dao.ResultWrapper.ResultStatus;
import tareas.model.AsignaturaInfo;
import tareas.model.Tarea;
import tareas.model.TipoCalificacionEnum;

public class TareaJPA implements TareaDAO {
	
	private Session session;

	public TareaJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createTarea(String titulo, String propietario, String openDate, String closeDate, AsignaturaInfo subject, Boolean permiteTexto,
			Boolean permitirAdjuntos, String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel) {
		
		Tarea ta = new Tarea();
		ta.setTitulo(titulo);
		ta.setPropietario(propietario);
		ta.setOpenDate(openDate);
		ta.setCloseDate(closeDate);
		ta.setSubject(subject);
		ta.setPermitirTexto(permiteTexto);
		ta.setPermitirAdjuntos(permitirAdjuntos);
		ta.setTipoCalificacion(TipoCalificacionEnum.valueOf(tipoCalificacion));
		ta.setInstrucciones(instrucciones);
		ta.setAccessLevel(accessLevel);
				
		try {
			session.beginTransaction();
			session.persist(ta);
			session.getTransaction().commit();
		
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + ta.toString()).build();
		} finally {
			session.close();
		}
		
		return ResultWrapper.statusOk().setObject(ta).build();
	}

	@Override
	public ResultWrapper findTareaById(String idTarea, String userId) {
		try{
			Tarea ta = session.find(Tarea.class, idTarea);
			if (ta == null) {
				return ResultWrapper.statusError().msg("La Tarea con id: " + idTarea + " no ha sido encontrada").build();
			}
			
			if(!ta.getPropietario().equals(userId) && !ta.getAlumnos().contains(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ userId)
									.build();
			}
			
			return ResultWrapper.statusOk().setObject(ta).build();
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAllTareas(String userId) {
		try {
			List<Tarea> tareas = session.createQuery("SELECT t FROM Tarea t "
					+ "WHERE t.propietario = :user "
					+ "OR :user MEMBER OF t.subject.alumnos", Tarea.class)
					.setParameter("user", userId)
					.getResultList();
			return ResultWrapper.statusOk().setObjects(tareas).build();
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper updateTarea(String idTarea, String titulo, String propietario, String openDate, String closeDate,
			String idAdjunto, String permiteTexto, String permitirAdjuntos,	String tipoCalificacion,
			String instrucciones, String infoAdicional, String accessLevel) {
		
		Tarea ta = session.find(Tarea.class, idTarea);
		
		if(!ta.getPropietario().equals(propietario)) {
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
								.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ propietario)
								.build();
		}
		
		if(StringUtils.isNotBlank(titulo) && !ta.getTitulo().equals(titulo)) ta.setTitulo(titulo);
		if(StringUtils.isNotBlank(propietario) && !ta.getPropietario().equals(propietario)) ta.setPropietario(propietario);
		if(StringUtils.isNotBlank(openDate) && !ta.getOpenDate().equals(openDate)) ta.setOpenDate(openDate);
		if(StringUtils.isNotBlank(closeDate) && !ta.getCloseDate().equals(closeDate)) ta.setCloseDate(closeDate);
		if(StringUtils.isNotBlank(idAdjunto)) ta.getIdAdjuntos().add(idAdjunto);
		
		if(StringUtils.isNotBlank(permiteTexto)) ta.setPermitirTexto(Boolean.parseBoolean(permiteTexto));
		if(StringUtils.isNotBlank(permitirAdjuntos)) ta.setPermitirAdjuntos(Boolean.parseBoolean(permitirAdjuntos));
		
		if(StringUtils.isNotBlank(tipoCalificacion) && !ta.getTipoCalificacion().equals(TipoCalificacionEnum.valueOf(tipoCalificacion))) ta.setTipoCalificacion(TipoCalificacionEnum.valueOf(tipoCalificacion));
		if(StringUtils.isNotBlank(instrucciones) && !ta.getInstrucciones().equals(instrucciones)) ta.setInstrucciones(instrucciones);
		if(StringUtils.isNotBlank(accessLevel) && !ta.getAccessLevel().equals(accessLevel)) ta.setAccessLevel(accessLevel);
		
		session.beginTransaction();
		try {
			session.update(ta);
			session.getTransaction().commit();
		
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al actualizar el registro: " + ta.toString()).build();
		} finally {
			session.close();
		}
		
		return ResultWrapper.statusOk().setObject(ta).build();
	}

	@Override
	public ResultWrapper remove(String idTarea, String userId) {
		try {
			Tarea ta = session.find(Tarea.class, idTarea);
						
			if (ta == null) {
				return ResultWrapper.statusError().msg("La Tarea con id: " + idTarea + " no ha sido encontrada").build();
			}
			
			if(!ta.getPropietario().equals(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ userId)
									.build();
			}			
			
			session.beginTransaction();
			session.delete(ta);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().build();
		
		} finally {
			session.close();
		}
	}
	
	@Override
	public ResultWrapper updateAttachmentField(String idTarea, String idAdjunto) {
		Tarea ta = session.find(Tarea.class, idTarea);
		
		ta.getIdAdjuntos().add(idAdjunto);
		
		session.beginTransaction();
		try {
			session.update(ta);
			session.getTransaction().commit();
		
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al actualizar el registro: " + ta.toString()).build();
		} finally {
			session.close();
		}
		
		return ResultWrapper.statusOk().setObject(ta).build();
	}

}
