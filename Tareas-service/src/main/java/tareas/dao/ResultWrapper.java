package tareas.dao;

import java.util.Collection;
import java.util.List;

public class ResultWrapper {

	private ResultStatus status;
	private String msg;
	private Object obj;
	private Collection<?> objs;
	
	private ResultWrapper() {}
	
	public ResultStatus getStatus() {
		return status;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public Object getObj() {
		return obj;
	}
	
	public Collection<?> getObjs(){
		return objs;
	}
	
	public static ResultWrapperBuilder statusOk() {
		return new ResultWrapperBuilder().status(ResultStatus.OK);
	}
	
	public static ResultWrapperBuilder statusError() {
		return new ResultWrapperBuilder().status(ResultStatus.ERROR);
	}
	
	public static ResultWrapperBuilder status(ResultStatus status) {
		return new ResultWrapperBuilder().status(status);
	}
	
	@Override
	public String toString() {
		return "ResultWrapper [status=" + status + ", msg=" + msg + ", obj=" + obj + ", objs=" + objs + "]";
	}

	public static class ResultWrapperBuilder{
		private ResultWrapper wrapper;
		
		private ResultWrapperBuilder() {
			wrapper = new ResultWrapper();
		}
		
		public ResultWrapperBuilder status(ResultStatus status) {
			this.wrapper.status = status;
			return this;
		}
		
		public ResultWrapperBuilder msg(String msg) {
			this.wrapper.msg = msg;
			return this;
		}
		
		public ResultWrapperBuilder setObject(Object obj) {
			this.wrapper.obj = obj;
			return this;
		}
		
		public ResultWrapperBuilder setObjects(List<?> objs) {
			this.wrapper.objs = objs;
			return this;
		}
		
		public ResultWrapper build() {
			return wrapper;
		}		
	}

	public enum ResultStatus {
		ERROR, OK, OVERLAP, EXCEPTION, EXISTS, FORBIDDEN;
	}

}
