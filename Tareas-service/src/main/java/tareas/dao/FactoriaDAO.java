package tareas.dao;

public interface FactoriaDAO {
	
	public TareaDAO getTareaDAO();
	public EnvioDAO getEnvioDAO();
	public CalificacionDAO getCalificacionDAO();
	public AsignaturaInfoDAO getAsignaturaInfoDAO();
	
}
