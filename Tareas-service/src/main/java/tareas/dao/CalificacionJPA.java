package tareas.dao;

import org.hibernate.Session;
import tareas.dao.ResultWrapper.ResultStatus;
import tareas.model.Calificacion;
import tareas.model.Envio;
import tareas.model.EstadoEnum;
import tareas.model.Tarea;

public class CalificacionJPA implements CalificacionDAO {

	private Session session;

	public CalificacionJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createCalificacion(String idTarea, String idSolucion, String calificador, float puntuacion,
			String comentarios) {
		try {			
			ResultWrapper rw = validateTaskAndSolutionProfesor(idTarea, idSolucion, calificador, false);
			
			if(rw.getStatus() != ResultStatus.OK) return rw;
			
			Envio en = (Envio) rw.getObj();
			
			Calificacion c = new Calificacion();
			c.setCalificador(calificador);
			c.setPuntuacion(puntuacion);
			c.setComentarios(comentarios);
			c.setEnvio(en);

			en.setEstado(EstadoEnum.CALIFICADA);
			
			session.beginTransaction();
			session.persist(c);
			session.persist(en);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().setObject(c).build(); 

		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
					.msg("Ha habido un problema al insertar el registro calificacion.").build();
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findCalificacionBySolution(String idTask, String idSolucion, String userId) {
		try {
			ResultWrapper rw = validateTaskAndSolution(idTask, idSolucion, userId);	
			
			if(rw.getStatus() != ResultStatus.OK) return rw;
			
			Envio en = (Envio) rw.getObj();			
			return ResultWrapper.statusOk().setObject(en.getCalificacion()).build();
	
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper updateCalificacion(String task, String idSolucion, float puntuacion, String comentarios, String userId) {		
		try {
			ResultWrapper rw = validateTaskAndSolutionProfesor(task, idSolucion, userId, true);
			
			if(rw.getStatus() != ResultStatus.OK) return rw;
			
			Calificacion c = ((Envio) rw.getObj()).getCalificacion();
			if (c.getPuntuacion() != puntuacion) c.setPuntuacion(puntuacion);
			if (!c.getComentarios().equals(comentarios)) c.setComentarios(comentarios);
			
			session.beginTransaction();			
			session.persist(c);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().setObject(c).build();
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
					.msg("Ha habido un problema al insertar el registro Calificacion").build();
		} finally {
			session.close();
		}
		
	}
	
	private ResultWrapper validateTaskAndSolutionProfesor(String idTarea, String idSolucion, String calificador, boolean isUpdate) {
		Tarea ta = session.find(Tarea.class, idTarea);
		
		if(ta == null) {
			return ResultWrapper.statusError()
								.msg("La tarea con id: " + idTarea + " no ha sido encontrada")
								.build();
		}
		
		if(!ta.getPropietario().equals(calificador)) {
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
					.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ calificador)
					.build();
		}
		
		Envio en = session.find(Envio.class, idSolucion);
		
		if(en == null) {
			return ResultWrapper.statusError()
								.msg("El envio con id: " + idSolucion + " no ha sido encontrado")
								.build();
		}
		
		if(!ta.getEnvios().contains(en)) {
			return ResultWrapper.statusError()
								.msg("El envio con id: " + idSolucion + " no pertenece a la tarea especificada")
								.build();
		}
		
		if(isUpdate && en.getCalificacion() == null) {
			return ResultWrapper.statusError().msg("El envio con id: " + idSolucion + " no ha sido calificado.").build();
		}
		
		if (!isUpdate && en.getCalificacion() != null) {
			return ResultWrapper.statusError().msg("El envio con id: " + idSolucion + " ya ha sido calificado.").build();
		}
		
		return ResultWrapper.statusOk().setObject(en).build();
	}
	
	private ResultWrapper validateTaskAndSolution(String idTarea, String idSolucion, String usuario) {
		Tarea ta = session.find(Tarea.class, idTarea);
		
		if(ta == null) {
			return ResultWrapper.statusError()
								.msg("La tarea con id: " + idTarea + " no ha sido encontrada")
								.build();
		}
		
		if(!ta.getPropietario().equals(usuario) && !ta.getAlumnos().contains(usuario)) {
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
					.msg("La Tarea con id: " + idTarea + " no esta disponible para el usuario: "+ usuario)
					.build();
		}
		
		Envio en = session.find(Envio.class, idSolucion);
		
		if(en == null) {
			return ResultWrapper.statusError()
								.msg("El envio con id: " + idSolucion + " no ha sido encontrado")
								.build();
		}
		
		if(!ta.getEnvios().contains(en)) {
			return ResultWrapper.statusError()
								.msg("El envio con id: " + idSolucion + " no pertenece a la tarea especificada")
								.build();
		}
		
		if(!ta.getPropietario().equals(usuario) && !en.getUsuario().equals(usuario)) {
			return ResultWrapper.status(ResultStatus.FORBIDDEN)
					.msg("El envio con id: " + idSolucion + " no pertenece al usuario: "+ usuario)
					.build();
		}
		
		if (en.getCalificacion() == null) {
			return ResultWrapper.statusError()
					.msg("El envio con id: " + idSolucion + " no tiene ninguna calificación asociada").build();
		}
		
		return ResultWrapper.statusOk().setObject(en).build();	
	}

}
