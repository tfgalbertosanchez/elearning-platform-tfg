package tareas.rabbitMQ;

public enum EventTypes {
	NUEVA_FECHA("nueva-fecha-event"), 
	NUEVA_TAREA("nueva-tarea-event"), 
	TAREA_COMPLETADA("tarea-completada-event"),
	ELIMINAR_FECHA("eliminar-fecha-event"),
	ELIMINAR_TAREA("eliminar-tarea-event");
    
    public final String label;

    private EventTypes(String label) {
        this.label = label;
    }
    
    @Override
    public String toString(){
        return label;
    }
}
