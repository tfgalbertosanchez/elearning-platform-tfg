package tareas.api;

import java.io.InputStream;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.header.FormDataContentDisposition;

public interface TareasApiService {
	
	//Manage Tasks
	public Response createTask(String titulo, String propietario, String openDate, String closeDate, String subjectCode, String permiteTexto, String permitirAdjuntos, String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) throws NotFoundException;
	public Response getTasks(String userId) throws NotFoundException;
	public Response getTask(String idTarea, String userId) throws NotFoundException;
	public Response updateTask(String idTarea, String titulo, String propietario, String openDate, String closeDate, String permiteTexto, String permitirAdjuntos, String tipoCalificacion, String instrucciones, String infoAdicional, String accessLevel, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) throws NotFoundException;
    public Response removeTask(String idTarea, String userId) throws NotFoundException;
	
	//Manage Solutions
    public Response createSolution(String idTarea, String usuario, String solucionTexto, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) throws NotFoundException;
    public Response getSolutions(String idTarea, String userId) throws NotFoundException;
    public Response getSolution(String idTarea, String idSolucion, String userId) throws NotFoundException;
    public Response updateSolution(String idTarea ,String idSolucion, String solucionTexto, InputStream uploadedInputStream, FormDataContentDisposition fileDetail, String userId) throws NotFoundException;
   
	//Manage Grades	
	public Response createGrade(String idTarea, String idSolucion, String calificador, float puntuacion, String comentarios) throws NotFoundException;
	public Response getGrade(String idTarea, String idSolucion, String userId) throws NotFoundException;
	public Response updateGrade(String idTarea, String idSolucion, float puntuacion, String comentarios, String userId) throws NotFoundException;
    
}
