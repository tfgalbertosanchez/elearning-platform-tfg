package tareas.api;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import io.swagger.annotations.ApiParam;
import tareas.controller.TareaServiceController;
import tareas.security.AvailableRoles;
import tareas.security.Secured;

@Path("/tareas")
public class TareasApi{
	
	private final TareasApiService service = new TareaServiceController();
	
	private static String userId;
	
	private static String authorization;
	
	public static void setUserId(String currentUser) {
		userId = currentUser;
	}
	
	public static void setAuthorization(String token) {
		authorization = token;
	}
	
	public static String getAuthorization() {
		return authorization;
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response createTask(
			@FormDataParam("titulo") String titulo,
			@FormDataParam("openDate") String openDate,
			@FormDataParam("closeDate") String closeDate,
			@FormDataParam("subjectCode") String subjectCode,
			@DefaultValue("true") @FormDataParam("permiteTexto") String permiteTexto,
			@DefaultValue("false") @FormDataParam("permitirAdjuntos") String permitirAdjuntos,
			@DefaultValue("CALIFICADA") @FormDataParam("tipoCalificacion") String tipoCalificacion,
			@DefaultValue("") @FormDataParam("instrucciones") String instrucciones,
			@DefaultValue("") @FormDataParam("infoAdicional") String infoAdicional,
			@DefaultValue("publico") @FormDataParam("accessLevel") String accessLevel,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail)
			throws NotFoundException {
		
		return service.createTask(titulo, userId, openDate, closeDate, subjectCode, permiteTexto, permitirAdjuntos,
								  tipoCalificacion, instrucciones, infoAdicional, accessLevel, uploadedInputStream, fileDetail);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getTasks() throws NotFoundException {
		return service.getTasks(userId);
	}
	
	@GET
	@Path("/{idTarea}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getTask(
			@ApiParam(value = "ID de la tarea que se quiere consultar (TA0001)", required = true) @PathParam("idTarea") String idTarea)
			throws NotFoundException {
		
		return service.getTask(idTarea, userId);
	}
	
	@PUT
	@Path("/{idTarea}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response updateTask(
			@PathParam("idTarea") String idTarea,
			@FormDataParam("titulo") String titulo,
			@FormDataParam("openDate") String openDate,
			@FormDataParam("closeDate") String closeDate,
			@FormDataParam("permitirTexto") String permiteTexto,
			@FormDataParam("permitirAdjuntos") String permitirAdjuntos,
			@FormDataParam("tipoCalificacion") String tipoCalificacion,
			@FormDataParam("instrucciones") String instrucciones,
			@FormDataParam("infoAdicional") String infoAdicional,
			@FormDataParam("accessLevel") String accessLevel,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail) 
			throws NotFoundException {
		
		return service.updateTask(idTarea, titulo, userId, openDate, closeDate, permiteTexto, permitirAdjuntos, 
								  tipoCalificacion, instrucciones, infoAdicional, accessLevel, uploadedInputStream, fileDetail);
	}
	
	@DELETE
	@Path("/{idTarea}")
	@Secured({AvailableRoles.PROFESOR})
	public Response removeTask(
			@ApiParam(value = "ID de la tarea que se quiere eliminar (TA0001)", required = true) @PathParam("idTarea") String idTarea)
			throws NotFoundException {
		
		return service.removeTask(idTarea, userId);
	}

	@POST
	@Path("/{idTarea}/envio")
	@Consumes(MediaType.MULTIPART_FORM_DATA)  
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response createSolution(
			@PathParam("idTarea") String idTarea,
			@FormDataParam("solucionTexto") String solucionTexto,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail)
			throws NotFoundException {
		
		return service.createSolution(idTarea, userId, solucionTexto, uploadedInputStream, fileDetail);
	}
	
	@GET
	@Path("/{idTarea}/envio")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response getSolutions(
			@ApiParam(value = "ID de la tarea a la que se quiere añadir el envío (TA0001).", required = true) @PathParam("idTarea") String idTarea)
			throws NotFoundException {
		
		return service.getSolutions(idTarea, userId);
	}
	
	
	@GET
	@Path("/{idTarea}/{idSolucion}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response getSolution(
			@ApiParam(value = "ID de la tarea de la que se quiere consultar el envío (TA0001).", required = true) @PathParam("idTarea") String idTarea,
			@ApiParam(value = "ID del envío que se quiere consultar (EN0001).", required = true) @PathParam("idSolucion") String idSolucion)
			throws NotFoundException {
		
		return service.getSolution(idTarea, idSolucion, userId);
	}
	
	@PUT
	@Path("/{idTarea}/{idSolucion}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)  
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response updateSolution(
			@PathParam("idTarea") String idTarea,
			@PathParam("idSolucion") String idSolucion,
			@FormDataParam("solucionTexto") String solucionTexto,
			@FormDataParam("file") InputStream uploadedInputStream,  
            @FormDataParam("file") FormDataContentDisposition fileDetail)
			throws NotFoundException {
		
		return service.updateSolution(idTarea, idSolucion, solucionTexto, uploadedInputStream, fileDetail, userId);
	}
	
	@POST
	@Path("/{idTarea}/{idSolucion}/calificacion")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response createGrade(
			@ApiParam(value = "ID de la tarea a la que se quiere añadir el envío (TA0001).", required = true) @PathParam("idTarea") String idTarea,
			@ApiParam(value = "ID del envío que se quiere consultar (EN0001).", required = true) @PathParam("idSolucion") String idSolucion,
			//@ApiParam(value = "Usuario que califica la tarea.", required = true) @FormParam("calificador") String calificador,
			@ApiParam(value = "Puntuación obtenida en la tara.", required = true) @FormParam("puntuacion") float puntuacion,
			@ApiParam(value = "Comentarios añadidos a la tarea.") @FormParam("comentarios") String comentarios)
			throws NotFoundException {
		
		return service.createGrade(idTarea, idSolucion, userId, puntuacion, comentarios);
	}
	
	@GET
	@Path("/{idTarea}/{idSolucion}/calificacion")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR, AvailableRoles.ALUMNO})
	public Response getGrade(
			@ApiParam(value = "ID de la tarea a la que se quiere añadir el envío (TA0001).", required = true) @PathParam("idTarea") String idTarea,
			@ApiParam(value = "ID del envío que se quiere consultar (EN0001).", required = true) @PathParam("idSolucion") String idSolucion)
			throws NotFoundException {
		
		return service.getGrade(idTarea, idSolucion, userId);
	}
	
	@PUT
	@Path("/{idTarea}/{idSolucion}/calificacion")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response updateGrade(@ApiParam(value = "ID de la tarea a la que se quiere añadir el envío (TA0001).", required = true) @PathParam("idTarea") String idTarea,
			@ApiParam(value = "ID del envío que se quiere consultar (EN0001).", required = true) @PathParam("idSolucion") String idSolucion,
			@ApiParam(value = "Puntuación obtenida en la tarea.", required = true) @FormParam("puntuacion") float puntuacion,
			@ApiParam(value = "Comentarios añadidos a la tarea.") @FormParam("comentarios") String comentarios)
			throws NotFoundException {
		
		return service.updateGrade(idTarea, idSolucion, puntuacion, comentarios, userId);
	}	
}
