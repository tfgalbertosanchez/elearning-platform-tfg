package agenda.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import agenda.controller.AgendaServiceController;
import agenda.security.AvailableRoles;
import agenda.security.Secured;
import io.swagger.annotations.ApiParam;

@Path("/agenda")
public class AgendaApi{
	
	private final AgendaApiService service = AgendaServiceController.getInstance();
	
	private static String userId;
	private static String authorization;
	
	public static void setUserId(String currentUser) {
		userId = currentUser;
	}
	
	public static void setAuthorization(String currentJWT) {
		authorization = currentJWT;
	}
	
	public static String getAuthorization() {
		return authorization;
	}
	
	@POST
	@Path("/fechasClave")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response createKeyDate(
			@ApiParam(value = "titulo de la fecha clave", required = true) @FormParam("titulo") String title,
			@ApiParam(value = "fecha", required = true) @FormParam("fecha") String date,
			@ApiParam(value = "servicio que crea fecha clave") @FormParam("service") String service,
			@ApiParam(value = "id del elemento referido por la fecha clave") @FormParam("resourceId") String resourceId,
			@ApiParam(value = "codigo de la asignatura") @FormParam("subjectCode") String subjectCode)
		
			throws NotFoundException {		
		
		return this.service.createKeyDate(title, date, service, resourceId, subjectCode, userId);
	}
	
	@GET
	@Path("/fechasClave")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response getKeyDates() throws NotFoundException {
		return service.getKeyDates(userId);			
	}
	
	@GET
	@Path("/fechasClave/{idKeyDate}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response getKeyDate(
			@ApiParam(value = "ID de la fecha clave que se quiere consultar (FC0001)", required = true) @PathParam("idKeyDate") String idKeyDate)
			throws NotFoundException {
		
		return service.getKeyDate(idKeyDate, userId);
	}
	
		
	@DELETE
	@Path("/fechasClave/{idKeyDate}")
	@Secured({AvailableRoles.PROFESOR})
	public Response removeKeyDate(
			@ApiParam(value = "ID de la fecha clave que se quiere eliminar (FC0001)", required = true) @PathParam("idKeyDate") String idKeyDate)
			throws NotFoundException {
		
		return service.removeKeyDate(idKeyDate, userId);
	}

	@POST
	@Path("/tareasPendientes")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.PROFESOR})
	public Response createPendingTask(
			@ApiParam(value = "titulo de la tarea pendiente", required = true) @FormParam("titulo") String title,
			@ApiParam(value = "servicio que crea tarea pendiente") @FormParam("service") String service,			
			@ApiParam(value = "id del elemento referido por la tarea pendiente") @FormParam("resourceId") String resourceId,
			@ApiParam(value = "codigo de la asignatura") @FormParam("subjectCode") String subjectCode)			
			throws NotFoundException {
		
		return this.service.createPendingTask(title, service, resourceId, subjectCode, userId);
	}
	
	@GET
	@Path("/tareasPendientes")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response getPendingTasks() throws NotFoundException {
		return service.getPendingTasks(userId);
	}	
	
	@GET
	@Path("/tareasPendientes/{idTask}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured({AvailableRoles.ALUMNO})
	public Response getPendingTask(
			@ApiParam(value = "ID de la tarea pendiente que se quiere consultar (TP0001)", required = true) @PathParam("idTask") String idTask)
			throws NotFoundException {
		
		return service.getPendingTask(idTask, userId);
	}	
	
	@DELETE
	@Path("/tareasPendientes/{idTask}")
	@Secured({AvailableRoles.PROFESOR})
	public Response removeSolution(
			@ApiParam(value = "ID de la tarea pendiente que se quiere consultar (TP0001)", required = true) @PathParam("idTask") String idTask)
			throws NotFoundException {
		
		return service.removePendingTask(idTask, userId);
	}
	
}
