package agenda.api;

public class NotFoundException extends Exception {
   
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private int code;
	
    public NotFoundException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
