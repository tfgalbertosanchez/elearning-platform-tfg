package agenda.api;

import javax.ws.rs.core.Response;

public interface AgendaApiService {
	
	//Manage key dates
	public Response createKeyDate(String title, String date, String service, String resourceId, String subjectCode, String userId);
	public Response getKeyDates(String userId) throws NotFoundException;
	public Response getKeyDate(String idKeyDate, String userId) throws NotFoundException;
	public Response removeKeyDate(String idKeyDate, String userId) throws NotFoundException;
	
	//Manage pending tasks
	public Response createPendingTask(String title, String service, String resourceId, String subjectCode, String userId);
	public Response getPendingTasks(String userId) throws NotFoundException;
	public Response getPendingTask(String idTask, String userId) throws NotFoundException;
	public Response removePendingTask(String idTask, String userId) throws NotFoundException;    
}
