package agenda.dao;

import java.util.List;

import org.hibernate.Session;

import agenda.dao.ResultWrapper.ResultStatus;
import agenda.model.TareasPendientes;

public class TareasPendientesJPA implements TareasPendientesDAO{
	
	private Session session;

	public TareasPendientesJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createTareaPendiente(String title, String service, String resourceId,
											String subjectCode, List<String> users) {
		TareasPendientes tp = null;
		
		try {			
			for(String userId : users) {
				tp = new TareasPendientes();
				tp.setTitle(title);
				tp.setService(service);
				tp.setResourceId(resourceId);
				tp.setSubjectCode(subjectCode);
				tp.setUserId(userId);
				
				session.beginTransaction();
				session.persist(tp);
				session.getTransaction().commit();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro: " + tp.toString()).build();
		} finally {
			session.close();
		}
			
		return ResultWrapper.statusOk().setObject(tp).build();
		
	}

	@Override
	public ResultWrapper findTareaPendiente(String id, String userId) {
		try{
			TareasPendientes tp = session.find(TareasPendientes.class, id);
			if (tp == null) {
				return ResultWrapper.statusError().msg("La Tarea pendiente con id: " + id + " no ha sido encontrada").build();
				
			}else if(!tp.getUserId().equals(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
						.msg("La Tarea Pendiente con id: " + id + " no esta disponible para el usuario: "+ userId)
						.build();
			} else {
				return ResultWrapper.statusOk().setObject(tp).build();
			}		
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAll() {
		try {
			List<TareasPendientes> tps = session.createQuery("SELECT t FROM TareasPendientes t", TareasPendientes.class).getResultList();
			return ResultWrapper.statusOk().setObjects(tps).build();
			
		} finally {
			session.close();
		}
	}

	@Override
	public String findSubjectCode(String id) {
		TareasPendientes tp = session.find(TareasPendientes.class, id);
		if(tp != null) {
			return tp.getSubjectCode();
		} else {
			return "";
		}
	}
	
	@Override
	public ResultWrapper findByUserId(String userId) {
		List<TareasPendientes> tasks = session.createQuery("SELECT t FROM TareasPendientes t WHERE userId = :user", TareasPendientes.class)
										 .setParameter("user", userId)
										 .getResultList();
		return ResultWrapper.statusOk().setObjects(tasks).build();
	}

	@Override
	public ResultWrapper remove(String id) {
		try {
			TareasPendientes tp = session.find(TareasPendientes.class, id);
			
			if (tp == null) {
				return ResultWrapper.statusError().msg("La Tarea Pendiente con id: " + id + " no ha sido encontrada").build();
			}
			
			session.beginTransaction();
			session.delete(tp);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().build();
		
		} finally {
			session.close();
		}
	}
	
	@Override
	public void removeByResourceId(String resourceId, String user) {
		TareasPendientes tp = session.createQuery("SELECT t FROM TareasPendientes t WHERE " 
							+ "resourceId = :resourceId AND userId = :user", TareasPendientes.class)
									 .setParameter("resourceId", resourceId)
									 .setParameter("user", user)
									 .getResultList()
									 .get(0);
		session.beginTransaction();
		session.delete(tp);
		session.getTransaction().commit();
		
	}
	
	@Override
	public void removeAllByResourceId(String resourceId) {
		session.createQuery("DELETE FROM TareasPendientes t WHERE resourceId = :resourceId")
				.setParameter("resourceId", resourceId)
				.executeUpdate();
	}
}
