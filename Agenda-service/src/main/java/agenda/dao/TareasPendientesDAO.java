package agenda.dao;

import java.util.List;

public interface TareasPendientesDAO {
	
	public ResultWrapper createTareaPendiente(String title, String service, String resourceId, String subjectCode, List<String> users);
	public ResultWrapper findTareaPendiente(String id, String userId);
	public ResultWrapper findAll();
	public String findSubjectCode(String id);
	public ResultWrapper findByUserId(String userId);
	public ResultWrapper remove(String id);
	public void removeByResourceId(String resourceId, String userId);
	public void removeAllByResourceId(String resourceId);
}
