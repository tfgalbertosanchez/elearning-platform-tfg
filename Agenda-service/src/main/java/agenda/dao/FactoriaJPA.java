package agenda.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class FactoriaJPA implements FactoriaDAO{

	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;
		
	public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
            	registry = new StandardServiceRegistryBuilder().configure().build();
                MetadataSources sources = new MetadataSources(registry);
                Metadata metadata = sources.getMetadataBuilder().build();
                sessionFactory = metadata.getSessionFactoryBuilder().build();

            } catch (Exception e) {
                e.printStackTrace();
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return sessionFactory;
    }

    public static void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

	@Override
	public FechasClaveDAO getFechasClaveDAO() {
		return new FechasClaveJPA(getSessionFactory().openSession());		
	}

	@Override
	public TareasPendientesDAO getTareasPendientesDAO() {
		return new TareasPendientesJPA(getSessionFactory().openSession());		
	}
	
	@Override
	public AsignaturaInfoDAO getAsignaturaInfoDAO() {
		return new AsignaturaInfoJPA(getSessionFactory().openSession());
	}

}
