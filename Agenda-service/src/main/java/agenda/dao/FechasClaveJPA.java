package agenda.dao;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Session;

import agenda.dao.ResultWrapper.ResultStatus;
import agenda.model.FechasClave;

public class FechasClaveJPA implements FechasClaveDAO {
	
	private Session session;

	public FechasClaveJPA(Session session) {
		this.session = session;
	}

	@Override
	public ResultWrapper createFechasClave(String title, String date, String service,
													String resourceId, String subjectCode, List<String> users) {
		FechasClave keyDate = null;
		
		try {	
			for(String userId : users) {	
				keyDate = new FechasClave();
				keyDate.setTitle(title);
				keyDate.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(date));
				keyDate.setService(service);
				keyDate.setResourceId(resourceId);
				keyDate.setSubjectCode(subjectCode);			
				keyDate.setUserId(userId);
				
				session.beginTransaction();
				session.persist(keyDate);
				session.getTransaction().commit();				
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return ResultWrapper.status(ResultStatus.EXCEPTION)
								.msg("Ha habido un problema al insertar el registro").build();
		} finally {
			session.close();
		}
			
		return ResultWrapper.statusOk().setObject(keyDate).build();
	}

	@Override
	public ResultWrapper findFechasClave(String id, String userId) {
		try{
			FechasClave keyDate = session.find(FechasClave.class, id);
			if (keyDate == null) {
				return ResultWrapper.statusError().msg("La Fecha Clave con id: " + id + " no ha sido encontrada").build();
			
			} else if(!keyDate.getUserId().equals(userId)) {
				return ResultWrapper.status(ResultStatus.FORBIDDEN)
									.msg("La Fecha Clave con id: " + id + " no esta disponible para el usuario: "+ userId)
									.build();
			} else {
				return ResultWrapper.statusOk().setObject(keyDate).build();
			}			
			
		} finally {
			session.close();
		}
	}

	@Override
	public ResultWrapper findAll() {
		try {
			List<FechasClave> dates = session.createQuery("SELECT f FROM FechasClave f", FechasClave.class).getResultList();
			return ResultWrapper.statusOk().setObjects(dates).build();
			
		} finally {
			session.close();
		}
	}
	
	@Override
	public String findSubjectCode(String id) {
		FechasClave f = session.find(FechasClave.class, id);
		if(f != null) {
			return f.getSubjectCode();
		} else {
			return "";
		}
	}
	
	@Override
	public ResultWrapper findByUserId(String userId) {
		List<FechasClave> dates = session.createQuery("SELECT f FROM FechasClave f WHERE userId = :user", FechasClave.class)
										 .setParameter("user", userId)
										 .getResultList();
		return ResultWrapper.statusOk().setObjects(dates).build();
	}
	
	@Override
	public ResultWrapper remove(String id) {
		try {
			FechasClave keyDate = session.find(FechasClave.class, id);
			
			if (keyDate == null) {
				return ResultWrapper.statusError().msg("La Fecha Clave con id: " + id + " no ha sido encontrada").build();
			}
			
			session.beginTransaction();
			session.delete(keyDate);
			session.getTransaction().commit();
			
			return ResultWrapper.statusOk().build();
		
		} finally {
			session.close();
		}
	}
	
	@Override
	public void removeByResourceId(String resourceId, String userId) {
		FechasClave date = session.createQuery("SELECT f FROM FechasClave f WHERE "
					+ "resourceId = :resourceId AND userId = :user", FechasClave.class)
								  .setParameter("resourceId", resourceId)
								  .setParameter("user", userId)
								  .getResultList()
								  .get(0);
		
		session.beginTransaction();
		session.delete(date);
		session.getTransaction().commit();
		
	}
	
	@Override
	public void removeAllByResourceId(String resourceId) {
		session.createQuery("DELETE FROM FechasClave f WHERE resourceId = :resourceId")
				.setParameter("resourceId", resourceId)
				.executeUpdate();
	}


}
