package agenda.dao;

import java.util.List;

import agenda.model.AsignaturaInfo;

public interface AsignaturaInfoDAO {
	public AsignaturaInfo create(String subjectCode, String profesor, List<String> alumnos);
	public AsignaturaInfo findById(String Id);
}
