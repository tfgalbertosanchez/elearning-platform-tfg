package agenda.dao;

public interface FactoriaDAO {
	
	public FechasClaveDAO getFechasClaveDAO();
	public TareasPendientesDAO getTareasPendientesDAO();
	public AsignaturaInfoDAO getAsignaturaInfoDAO();
	
}
