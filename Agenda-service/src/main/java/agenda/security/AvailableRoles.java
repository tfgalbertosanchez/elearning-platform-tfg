package agenda.security;

public enum AvailableRoles {
	ADMINISTRADOR, PROFESOR, ALUMNO;
}
