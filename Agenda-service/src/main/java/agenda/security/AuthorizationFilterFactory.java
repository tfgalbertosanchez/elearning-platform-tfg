package agenda.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

public class AuthorizationFilterFactory implements ResourceFilterFactory {

    @Override
    public List<ResourceFilter> create(AbstractMethod abstractMethod) {    	
    	//Only add Authorization filter if method is marked as @Secured
    	if(abstractMethod.isAnnotationPresent(Secured.class)) {
    		return Arrays.asList(new AuthorizationFilter(abstractMethod));
    	}
    	
    	//If there aren't available filters, returns a empty list
    	return new ArrayList<ResourceFilter>();    	
    }
}
