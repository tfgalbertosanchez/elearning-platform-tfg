package agenda.security;

import java.security.Key;
import java.util.Date;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtils {
	
	private static final String SECRET = "01u3J81mMk";
	private static final long EXPIRATION_TIME = 3600000; // 1 hour
	private static final String SERVICE = "Agenda service";
	
	public static String createJWT() {		
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	    
	    return Jwts.builder()
	    		   .setId(UUID.randomUUID().toString())
	    		   .setIssuer(SERVICE)
	    		   .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
	    		   .setSubject(SERVICE)
	    		   .claim("rol", AvailableRoles.ADMINISTRADOR)
	    		   .signWith(signatureAlgorithm, signingKey)
	    		   .compact();		
	}
}
