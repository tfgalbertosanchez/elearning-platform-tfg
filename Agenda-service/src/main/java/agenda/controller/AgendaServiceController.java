package agenda.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;

import agenda.api.AgendaApi;
import agenda.api.AgendaApiService;
import agenda.api.NotFoundException;
import agenda.dao.FactoriaDAO;
import agenda.dao.FactoriaJPA;
import agenda.dao.ResultWrapper;
import agenda.dao.ResultWrapper.ResultStatus;
import agenda.model.AsignaturaInfo;
import agenda.security.JwtUtils;

public class AgendaServiceController implements AgendaApiService {

	private static final String ASIGNATURAS_SERVICE_URL = "http://localhost:8094/api/asignaturas/";

	private static final String DATE_REGEX = "^(0[1-9]|[12]\\d|3[01])\\/(0[1-9]|1[0-2])\\/(\\d){4}$";
	private static AgendaServiceController instance = null;
	private FactoriaDAO factoria;

	private AgendaServiceController() {
		factoria = new FactoriaJPA();
	}

	public static AgendaServiceController getInstance() {
		if (instance == null) {
			instance = new AgendaServiceController();
		}

		return instance;
	}

	@Override
	public Response createKeyDate(String title, String date, String service, String resourceId, String subjectCode,
			String userId) {
		
		if (StringUtils.isBlank(title) || StringUtils.isBlank(date) || !date.matches(DATE_REGEX)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		List<String> enrolledStudents = getEnrolledStudents(userId, subjectCode);

		if (enrolledStudents == null) {
			return Response.status(Status.FORBIDDEN).entity("El usuario no pertenece a la asignatura " + subjectCode)
					.build();
		}

		ResultWrapper result = factoria.getFechasClaveDAO().createFechasClave(title, date, service, resourceId,
				subjectCode, enrolledStudents);

		if (result.getStatus() == ResultStatus.EXCEPTION) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(result.getMsg()).build();
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response getKeyDates(String userId) throws NotFoundException {
		ResultWrapper result = factoria.getFechasClaveDAO().findByUserId(userId);
		return Response.ok(result.getObjs()).build();
	}

	@Override
	public Response getKeyDate(String idKeyDate, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idKeyDate)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		ResultWrapper result = factoria.getFechasClaveDAO().findFechasClave(idKeyDate, userId);

		if (result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}

		if (result.getStatus() == ResultStatus.FORBIDDEN) {
			return Response.status(Response.Status.FORBIDDEN).entity(result.getMsg()).build();
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response removeKeyDate(String idKeyDate, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idKeyDate)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		String subjectCode = factoria.getFechasClaveDAO().findSubjectCode(idKeyDate);

		if (!checkMembership(userId, subjectCode)) {
			return Response.status(Status.FORBIDDEN).entity("El usuario no pertenece a la asignatura " + subjectCode)
					.build();
		}

		ResultWrapper result = factoria.getFechasClaveDAO().remove(idKeyDate);

		if (result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}

		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	@Override
	public Response createPendingTask(String title, String service, String resourceId, String subjectCode,
			String userId) {
		if (StringUtils.isBlank(title)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		List<String> enrolledStudents = getEnrolledStudents(userId, subjectCode);

		if (enrolledStudents == null) {
			return Response.status(Status.FORBIDDEN).entity("El usuario no pertenece a la asignatura " + subjectCode)
					.build();
		}

		ResultWrapper result = factoria.getTareasPendientesDAO().createTareaPendiente(title, service, resourceId,
				subjectCode, enrolledStudents);

		if (result.getStatus() == ResultStatus.EXCEPTION) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(result.getMsg()).build();
		}

		return Response.ok().entity(result.getObj()).build();
	}

	@Override
	public Response getPendingTasks(String userId) throws NotFoundException {
		ResultWrapper result = factoria.getTareasPendientesDAO().findByUserId(userId);
		return Response.ok(result.getObjs()).build();
	}

	@Override
	public Response getPendingTask(String idTask, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idTask)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		ResultWrapper result = factoria.getTareasPendientesDAO().findTareaPendiente(idTask, userId);

		if (result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}

		if (result.getStatus() == ResultStatus.FORBIDDEN) {
			return Response.status(Response.Status.FORBIDDEN).entity(result.getMsg()).build();
		}

		return Response.ok(result.getObj()).build();
	}

	@Override
	public Response removePendingTask(String idTask, String userId) throws NotFoundException {
		if (StringUtils.isBlank(idTask)) {
			return Response.status(Status.BAD_REQUEST).entity("Los parametros de la petici�n son incorrectos").build();
		}

		String subjectCode = factoria.getTareasPendientesDAO().findSubjectCode(idTask);

		if (!checkMembership(userId, subjectCode)) {
			return Response.status(Status.FORBIDDEN).entity("El usuario no pertenece a la asignatura " + subjectCode)
					.build();
		}

		ResultWrapper result = factoria.getTareasPendientesDAO().remove(idTask);

		if (result.getStatus() == ResultStatus.ERROR) {
			return Response.status(Status.NOT_FOUND).entity(result.getMsg()).build();
		}

		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	public void completeKeyDate(String resourceId, String userId) {
		factoria.getFechasClaveDAO().removeByResourceId(resourceId, userId);
	}

	public void removeKeyDateByResourceId(String resourceId) {
		factoria.getFechasClaveDAO().removeAllByResourceId(resourceId);
	}

	public void completePendingTask(String resourceId, String userId) {
		factoria.getTareasPendientesDAO().removeByResourceId(resourceId, userId);
	}

	public void removePendingTaskByResourceId(String resourceId) {
		factoria.getTareasPendientesDAO().removeAllByResourceId(resourceId);
	}
	
	private List<String> getEnrolledStudents(String profesor, String subjectCode) {
		AsignaturaInfo subject = factoria.getAsignaturaInfoDAO().findById(subjectCode);
		if(subject == null) {
			AsignaturaInfo asignatura = requestAsignaturaInfo(profesor, subjectCode);
			return (asignatura != null) ? asignatura.getAlumnos() : null;
			
		} else {
			return subject.getAlumnos();
		}
	}

	@SuppressWarnings("unchecked")
	private AsignaturaInfo requestAsignaturaInfo(String profesor, String subjectId) {
		String authorization = (AgendaApi.getAuthorization() != null)? AgendaApi.getAuthorization() : JwtUtils.createJWT();
		String result = executeCallout(ASIGNATURAS_SERVICE_URL + subjectId, authorization);	
		if (result != null) {
			try {
				HashMap<String, Object> resultMap = new ObjectMapper().readValue(result, HashMap.class);
				String profesorId = (String) resultMap.get("profesor");
				if (profesorId.equals(profesor)) {
					return factoria.getAsignaturaInfoDAO().create(resultMap.get("id").toString(), profesorId, (List<String>) resultMap.get("alumnos"));
				}

				return null;

			} catch (IOException e) {
				return null;
			}
		}
		return null;
	}

	private boolean checkMembership(String profesor, String subjectId) {
		return (getEnrolledStudents(profesor, subjectId) != null);
	}

	private String executeCallout(String url, String authorization) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader(HttpHeaders.AUTHORIZATION, authorization);
		HttpResponse httpresponse;

		try {
			httpresponse = httpclient.execute(httpget);
			if (httpresponse.getStatusLine().getStatusCode() == HttpServletResponse.SC_OK) {
				String text = new BufferedReader(
						new InputStreamReader(httpresponse.getEntity().getContent(), StandardCharsets.UTF_8)).lines()
								.collect(Collectors.joining("\n"));
				return text;
			}
			return null;

		} catch (UnsupportedOperationException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
