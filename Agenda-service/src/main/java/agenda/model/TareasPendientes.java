package agenda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import agenda.dao.CustomIdGenerator;

@Entity
@Table
@JsonPropertyOrder({ "id", "service", "title", "resourceId", "subjectCode"})
public class TareasPendientes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "tPendientes_seq")
	@GenericGenerator(name = "tPendientes_seq", strategy = "agenda.dao.CustomIdGenerator", parameters = {
			@Parameter(name = CustomIdGenerator.PREFIX_PARAMETER, value = "TP") })
	@Column(name = "id")
	private String id;

	@Column(name = "title")
	private String title;

	@Column(name = "service")
	private String service;

	@Column(name = "resourceId")
	private String resourceId;
	
	@Column(name = "subjectCode")
	private String subjectCode;
	
	@JsonIgnore
	@Column(name = "userId")
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getSubjectCode() {
		return subjectCode;
	}
	
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "TareasPendientes [id=" + id + ", title=" + title + ", service=" + service + ", resourceId=" + resourceId
				+ ", subjectCode=" + subjectCode + ", userId=" + userId+ "]";
	}
}
