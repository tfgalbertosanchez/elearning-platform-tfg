package agenda.rabbitMQ;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import agenda.controller.AgendaServiceController;

public class TareaCompletadaEventProcessor implements EventProcessor {

	@Override
	public void processEvent(String jsonMsg) {
		System.out.println("### TareaCompletadaEventProcessor");
		
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Map<String, String> eventMap = objectMapper.readValue(jsonMsg, new TypeReference<Map<String, String>>(){});			
			String service = eventMap.get("service");
			String resourceId = eventMap.get("resourceId");
			String userId = eventMap.get("userId");
			
			if(service.equals("Tareas-service") && resourceId.startsWith("TA")) {
				AgendaServiceController.getInstance().completePendingTask(resourceId, userId);
				AgendaServiceController.getInstance().completeKeyDate(resourceId, userId);
			
			} else {
				AgendaServiceController.getInstance().completePendingTask(resourceId, userId);
			}			
			
		} catch (IOException e) {
			System.err.println("Ha habido un error al procesar el evento");
			e.printStackTrace();
		}
	}

}
