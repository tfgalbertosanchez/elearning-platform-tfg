package agenda.rabbitMQ;

public interface EventProcessor {
	public void processEvent(String jsonMsg);
}
