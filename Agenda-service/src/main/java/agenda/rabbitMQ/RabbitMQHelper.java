package agenda.rabbitMQ;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class RabbitMQHelper {

	private static ConnectionFactory factory = null;

	private static ConnectionFactory getConnectionFactory() {
		if (factory == null) {
			try {
				factory = new ConnectionFactory();
				factory.setUri("amqp://eqmekjwy:wR_nZWMcUOuLDvqrLuuBxA24l44YtnY4@squid.rmq.cloudamqp.com/eqmekjwy");

			} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e) {
				e.printStackTrace();
				return null;
			}
		}

		return factory;
	}

	public static void createConsumer(String queueName, String consumerTag) { 
		System.out.println("### RabbitMQHelper - createConsumer: queueName: " + queueName + ", consumerTag: " + consumerTag);
		try {
			Connection connection = getConnectionFactory().newConnection();

			Channel channel = connection.createChannel();

			//parameters: queueName, durable, exclusive, autodelete and properties
			channel.queueDeclare(queueName, true, false, false, null);

			boolean autoAck = false;
			channel.basicConsume(queueName, autoAck, consumerTag, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					
					String msg = new String(body, "UTF-8");
					try {
						EventProcessor processor = (EventProcessor) Class.forName("agenda.rabbitMQ." + consumerTag).newInstance();
						processor.processEvent(msg);
						
					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
						e.printStackTrace();
					}

					// Confirma el procesamiento
					long deliveryTag = envelope.getDeliveryTag();
					channel.basicAck(deliveryTag, false);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
