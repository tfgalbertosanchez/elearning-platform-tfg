package agenda.rabbitMQ;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import agenda.controller.AgendaServiceController;

public class EliminarFechaEventProcessor implements EventProcessor {

	@Override
	public void processEvent(String jsonMsg) {
		System.out.println("### Recived eliminar-fecha-event ###");

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Map<String, String> eventMap = objectMapper.readValue(jsonMsg, new TypeReference<Map<String, String>>(){});
			AgendaServiceController.getInstance().removeKeyDateByResourceId(eventMap.get("resourceId"));	

		} catch (IOException e) {
			System.err.println("Ha habido un error al procesar el evento");
			e.printStackTrace();
		}
	}
}
