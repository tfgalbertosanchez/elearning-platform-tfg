package agenda.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import agenda.rabbitMQ.RabbitMQHelper;

public class ServiceConfigSetup implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("### Servlet Setup");

		// ConsumerTag is used by client libraries to determine what handler to invoke for a given delivery.
		// Font: https://www.rabbitmq.com/consumers.html#consumer-tags
		RabbitMQHelper.createConsumer("nueva-fecha-event", "NuevaFechaEventProcessor");
		RabbitMQHelper.createConsumer("nueva-tarea-event", "NuevaTareaEventProcessor");
		RabbitMQHelper.createConsumer("tarea-completada-event", "TareaCompletadaEventProcessor");
		RabbitMQHelper.createConsumer("eliminar-fecha-event", "EliminarFechaEventProcessor");
		RabbitMQHelper.createConsumer("eliminar-tarea-event", "EliminarTareaEventProcessor");

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
}
