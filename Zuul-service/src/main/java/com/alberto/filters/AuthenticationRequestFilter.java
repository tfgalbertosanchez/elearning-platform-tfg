package com.alberto.filters;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.alberto.config.JwtUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import net.minidev.json.JSONObject;

@Component
public class AuthenticationRequestFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		System.out.println("### Start filter ###");

		final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		String jwt = null;

		// JWT Token is in the form "Bearer token". Remove Bearer word and get only the Token
		if (authHeader != null && authHeader.startsWith("Bearer ")) {			
			jwt = authHeader.substring(7);

			try {
				// if token is valid configure Spring Security to manually set authentication
				Claims claims = JwtUtils.validateJWT(jwt);
				
				//Set user role privileges in Spring Security Auth
				ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority(claims.get("rol").toString()));

				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
						
				// After setting the Authentication in the context, we specify
				// that the current user is authenticated. So it passes the
				// Spring Security Configurations successfully.
				SecurityContextHolder.getContext().setAuthentication(auth);				
			
			//Catch all validation exceptions and return forbidden
			} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
				response.resetBuffer();
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.setHeader("Content-Type", "application/json");
				response.getOutputStream().write(buildLoginErrorJSON(e.getClass().getSimpleName(), e.getMessage()).getBytes());
				response.flushBuffer(); // marks response as committed
				return;
			}			
		}
		
		chain.doFilter(request, response);
	}
	
	private String buildLoginErrorJSON(String cause, String msg) {
		return new JSONObject()
				.appendField("Error", "Unathorized")
				.appendField("Description","El token de autenticación no es valido")
				.appendField("Cause", cause)
				.appendField("Message", msg)
				.toJSONString();
	}
}