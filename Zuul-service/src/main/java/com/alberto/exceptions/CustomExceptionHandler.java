package com.alberto.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomExceptionHandler implements ErrorController {
 
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/error", produces = "application/json")
    public @ResponseBody ResponseEntity error(HttpServletRequest request) { 
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body("Error al procesar la solicitud.");
    }

	@Override
	public String getErrorPath() {
		return "/error";
	}
}