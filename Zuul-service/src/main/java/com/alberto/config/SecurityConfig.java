package com.alberto.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import com.alberto.filters.AuthenticationRequestFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {	

	@Autowired
	SecuritySuccessHandler successHandler;
	
	@Autowired
	private AuthenticationRequestFilter authenticationRequestFilter;
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.httpBasic().disable()
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, "/usuarios").permitAll()
			.anyRequest().authenticated()
			.and()
			.oauth2Login().successHandler(successHandler)
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.addFilterBefore(authenticationRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}	
}
