package com.alberto.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.minidev.json.JSONObject;

@Component
public class SecuritySuccessHandler implements AuthenticationSuccessHandler {
	// URL USUARIOS SERVICE
	public static final String USUARIOS_URI = "http://localhost:8093/api/usuarios/";
	public static final String GATEWAY_URI = "http://localhost:8080";

	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String FORWARDED_HEADER = "X-Forwarded-Host";

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {

		System.out.println("## Start success handler ##");

		// Get user who is logged in
		String userId = ((DefaultOidcUser) authentication.getPrincipal()).getEmail();

		// fetch user info in Users service
		Map<String, String> result = fetchUserInfo(userId);

		// Build JWT to be used as authorization
		if (result != null) {
			String jwt = JwtUtils.createJWT(authentication.getName(), userId, result);

			String responseBody = buildResponseJSON(jwt);

			// add to response header
			response.addHeader(AUTHORIZATION_HEADER, TOKEN_PREFIX + jwt);

			// write response
			response.getOutputStream().write(responseBody.getBytes());

		} else {
			//If the user does not exist in the system invalidate login
			response.resetBuffer();
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.setHeader("Content-Type", "application/json");
			response.getOutputStream().write(buildLoginErrorJSON(userId).getBytes());
			response.flushBuffer(); // marks response as committed
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> fetchUserInfo(String userId) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(USUARIOS_URI + userId);
		httpget.setHeader(FORWARDED_HEADER, GATEWAY_URI);
		HttpResponse httpresponse;

		try {
			httpresponse = httpclient.execute(httpget);
			if (httpresponse.getStatusLine().getStatusCode() == HttpServletResponse.SC_OK) {
				String text = new BufferedReader(
						new InputStreamReader(httpresponse.getEntity().getContent(), StandardCharsets.UTF_8))
							.lines()
							.collect(Collectors.joining("\n"));

				return new ObjectMapper().readValue(text, HashMap.class);

			} else {
				// If status code != 200 return null
				return null;
			}

		} catch (UnsupportedOperationException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String buildResponseJSON(String jwt) {
		return new JSONObject()
				.appendField("status", "Authorized")
				.appendField("token_type", TOKEN_PREFIX)
				.appendField("token", jwt)
				.appendField("expires_in", "3600")
				.toJSONString();
	}

	private String buildLoginErrorJSON(String userName) {
		return new JSONObject()
				.appendField("Error", "Unathorized")
				.appendField("Description","El usuario " + userName
								+ " no existe en el sistema, por favor registrese antes de iniciar sesión.")
				.toJSONString();
	}

}