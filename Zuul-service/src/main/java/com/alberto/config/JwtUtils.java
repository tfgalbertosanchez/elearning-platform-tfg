package com.alberto.config;

import java.security.Key;
import java.util.Date;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JwtUtils {
	
	private static final String SECRET = "01u3J81mMk";
	private static final long EXPIRATION_TIME = 3600000; // 1 hour
	private static final String ISSUER = "Authentication service";
	
	public static String createJWT(String id, String userId, Map<String, String> userInfo) {		
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	    
	    return Jwts.builder()
	    		   .setId(id)
	    		   .setIssuer(ISSUER)
	    		   .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
	    		   .setSubject(userId)
	    		   .claim("name", userInfo.get("nombre") + " " + userInfo.get("apellidos"))
	    		   .claim("rol", userInfo.get("rol"))
	    		   .signWith(signatureAlgorithm, signingKey)
	    		   .compact();		
	}
	
	public static Claims validateJWT(String jwt) throws ExpiredJwtException, UnsupportedJwtException, 
													MalformedJwtException, SignatureException, IllegalArgumentException{
		//This line will throw an exception if it is not a signed JWS (as expected), has malformed claims or 
		// is expired
		return Jwts.parser()         
   			   	   .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET))
   			   	   .parseClaimsJws(jwt).getBody();		
	}
}
